package com.teamit.smartgarage.models.dtos;

public class FilterAssistanceDTO extends AssistanceDTO {

    private String sort;

    private String order;

    public FilterAssistanceDTO() {
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}