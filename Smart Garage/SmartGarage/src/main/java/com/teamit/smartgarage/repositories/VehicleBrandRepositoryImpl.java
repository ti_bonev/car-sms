package com.teamit.smartgarage.repositories;

import com.teamit.smartgarage.models.VehicleBrand;
import com.teamit.smartgarage.repositories.contracts.VehicleBrandRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class VehicleBrandRepositoryImpl extends AbstractCRUDRepository<VehicleBrand> implements VehicleBrandRepository {

    @Autowired
    public VehicleBrandRepositoryImpl(SessionFactory sessionFactory) {
        super(VehicleBrand.class, sessionFactory);
    }

    @Override
    public VehicleBrand getByName(String name) {
        return getByField("name", name);
    }

    @Override
    public List<VehicleBrand> search(Optional<String> search) {
        try (Session session = sessionFactory.openSession()) {
            if (search.isEmpty()) {
                return getAll();
            }
            Query<VehicleBrand> query = session.createQuery(" from VehicleBrand where " +
                    " name like :search ", VehicleBrand.class);
            query.setParameter("search", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<VehicleBrand> filter(Optional<String> sort, Optional<String> order) {
        try (Session session = sessionFactory.openSession()) {
            var queryBuild = new StringBuilder(" from VehicleBrand ");
            var queryParams = new HashMap<String, Object>();
            sort.ifPresent(value -> queryBuild.append(sortBy(value, order)));
            Query<VehicleBrand> query = session.createQuery(queryBuild.toString(), VehicleBrand.class);
            query.setProperties(queryParams);
            return query.list();
        }
    }

    private String sortBy(String value, Optional<String> order) {
        var queryBuild = new StringBuilder(" order by ");
        if ("brand".equals(value)) {
            queryBuild.append(" name ");
        } else {
            throw new UnsupportedOperationException("Type mismatch. Choose 'brand'.");
        }
        order.ifPresent(sortOrder -> {
            switch (sortOrder) {
                case "ascending":
                    queryBuild.append(" asc ");
                    break;
                case "descending":
                    queryBuild.append(" desc ");
                    break;
                default:
                    throw new UnsupportedOperationException(
                            "Type mismatch. Choose 'ascending' or 'descending'."
                    );
            }
        });
        return queryBuild.toString();
    }
}
