package com.teamit.smartgarage.models.dtos;

public class ServiceToVisitDTO {

    private long id;

    private String action;

    public ServiceToVisitDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
