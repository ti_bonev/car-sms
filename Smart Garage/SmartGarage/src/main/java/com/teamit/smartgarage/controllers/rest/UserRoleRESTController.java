package com.teamit.smartgarage.controllers.rest;

import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.UserRole;
import com.teamit.smartgarage.services.contracts.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("garage/users/roles")
public class UserRoleRESTController {
    private final UserRoleService userRoleService;

    @Autowired
    public UserRoleRESTController(UserRoleService userRoleService) {
        this.userRoleService = userRoleService;
    }

    @GetMapping
    public List<UserRole> getAll() {
        return userRoleService.getAll();
    }

    @GetMapping("/{id}")
    public UserRole getByID(@PathVariable long id) {
        try {
            return userRoleService.getByID(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/test")
    public UserRole getByName(@RequestParam String role) {
        try {
            return userRoleService.getByName(role);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
