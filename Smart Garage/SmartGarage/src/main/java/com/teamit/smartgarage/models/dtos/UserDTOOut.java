package com.teamit.smartgarage.models.dtos;

import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.Vehicle;
import com.teamit.smartgarage.models.Visit;

import java.util.List;

public class UserDTOOut extends User {

    private List<Visit> visits;
    private List<Vehicle> vehicles;

    public UserDTOOut() {
    }

    public List<Visit> getVisits() {
        return visits;
    }

    public void setVisits(List<Visit> visits) {
        this.visits = visits;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }
}