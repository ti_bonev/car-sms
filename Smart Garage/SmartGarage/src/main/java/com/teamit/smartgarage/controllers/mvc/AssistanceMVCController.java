package com.teamit.smartgarage.controllers.mvc;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.Assistance;
import com.teamit.smartgarage.models.dtos.AssistanceDTO;
import com.teamit.smartgarage.models.dtos.FilterAssistanceDTO;
import com.teamit.smartgarage.models.dtos.SearchDTO;
import com.teamit.smartgarage.services.contracts.AssistanceService;
import com.teamit.smartgarage.utils.mappers.AssistanceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/v1/assistance")
public class AssistanceMVCController {

    private final AssistanceService assistanceService;
    private final AssistanceMapper assistanceMapper;

    @Autowired
    public AssistanceMVCController(AssistanceService assistanceService,
                                   AssistanceMapper assistanceMapper) {
        this.assistanceService = assistanceService;
        this.assistanceMapper = assistanceMapper;
    }

    @ModelAttribute("search")
    public SearchDTO populateSearch() {
        return new SearchDTO();
    }

    @ModelAttribute("filter")
    public FilterAssistanceDTO populateFilter() {
        return new FilterAssistanceDTO();
    }

    @ModelAttribute("searchEndPoint")
    public String populateSearchEndPoint() {
        return "/v1/assistance/search";
    }

    @ModelAttribute("filterEndPoint")
    public String populateFilterEndPoint() {
        return "/v1/assistance/filter";
    }

    @GetMapping
    public String showAssistanceAllPage(Model model) {
        model.addAttribute("assistance", assistanceService.getAll());
        return "assistance-all";
    }

    @PostMapping("/search")
    public String searchAssistance(@Valid @ModelAttribute("search") SearchDTO searchDTO,
                                   BindingResult bindingResult,
                                   Model model) {
        if (bindingResult.hasErrors())
            return "assistance-all";
        var searchResult = assistanceService.search(
                Optional.ofNullable(searchDTO.getKeyword().isBlank() ? null : searchDTO.getKeyword())
        );
        model.addAttribute("assistance", searchResult);
        return "assistance-all";
    }

    @PostMapping("/filter")
    public String filterUsers(@Valid @ModelAttribute("filter") FilterAssistanceDTO filterAssistanceDTO,
                              BindingResult bindingResult,
                              Model model) {
//        if (bindingResult.hasErrors())
//            return "assistance-all";
//        var filterResult = assistanceService.filter(
//                Optional.ofNullable(filterAssistanceDTO.getName().isBlank() ? null : filterAssistanceDTO.getName()),
//                Optional.of(filterAssistanceDTO.getPrice()),
//                Optional.ofNullable(filterAssistanceDTO.getSort().isBlank() ? null : filterAssistanceDTO.getSort()),
//                Optional.ofNullable(filterAssistanceDTO.getOrder().isBlank() ? null : filterAssistanceDTO.getOrder())
//        );
//        model.addAttribute("assistance", filterResult);
//        return "assistance-all";
        return "not-implemented";
    }

    @GetMapping("/{id}")
    public String showSingleAssistancePage(@PathVariable long id,
                                           Model model) {
        try {
            Assistance assistance = assistanceService.getById(id);
            model.addAttribute("assistance", assistance);
            return "assistance";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showCreateAssistancePage(Model model) {
        model.addAttribute("assistance", new AssistanceDTO());
        return "assistance-new-update";
    }

    @PostMapping("/new")
    public String createAssistance(@Valid @ModelAttribute("assistance") AssistanceDTO assistanceDTO,
                                   BindingResult errors) {
        if (errors.hasErrors())
            return "assistance-new-update";
        try {
            Assistance assistance = assistanceMapper.fromDTO(assistanceDTO);
            assistanceService.create(assistance);
            return "redirect:/v1/assistance";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate.name", e.getMessage());
            return "assistance-new-update";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdateAssistancePage(@PathVariable long id,
                                           Model model) {
        try {
            Assistance assistance = assistanceService.getById(id);
            AssistanceDTO assistanceDTO = assistanceMapper.toDTO(assistance);
            model.addAttribute("assistance", assistanceDTO);
            return "assistance-new-update";
            //todo assistance-update.html view - if we rename Submit button to Create and Update.
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateAssistance(@PathVariable long id,
                                   @Valid @ModelAttribute("assistance") AssistanceDTO assistanceDTO,
                                   BindingResult errors,
                                   Model model) {
        if (errors.hasErrors())
            return "assistance-new-update";
        try {
            Assistance assistance = assistanceMapper.fromDTO(id, assistanceDTO);
            assistanceService.update(assistance);
            return "redirect:/v1/assistance";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate.name", e.getMessage());
            return "assistance-new-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteAssistance(@PathVariable long id,
                                   Model model) {
        //todo soft delete
        // delete methods don't work with @DeleteMapping because html expects @GetMapping. Why?
        try {
            assistanceService.delete(id);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        }
        return "redirect:/v1/assistance";
    }
}