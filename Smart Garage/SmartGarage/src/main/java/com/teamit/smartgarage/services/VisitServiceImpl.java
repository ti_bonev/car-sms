package com.teamit.smartgarage.services;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.exceptions.UnauthorizedOperationException;
import com.teamit.smartgarage.models.Assistance;
import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.Visit;
import com.teamit.smartgarage.repositories.contracts.VisitRepository;
import com.teamit.smartgarage.services.contracts.AssistanceService;
import com.teamit.smartgarage.services.contracts.MailService;
import com.teamit.smartgarage.services.contracts.VisitService;
import com.teamit.smartgarage.utils.currencyconverter.CurrencyConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class VisitServiceImpl implements VisitService {

    public static final String EMAIL_FOOTER = "\n" +
            "Thank you for choosing our services!\n" +
            "\n" +
            "Mighty IT Team\n" +
            "Ivo & Tosho\n" +
            "\n" +
            "Ivaylo Ivanov : 0885 63 20 31\n" +
            "Todor Bonev   : 0878 54 55 02";
    private final VisitRepository visitRepository;
    private final AssistanceService assistanceService;
    private final MailService mailService;

    @Autowired
    public VisitServiceImpl(VisitRepository visitRepository,
                            AssistanceService assistanceService,
                            MailService mailService) {
        this.visitRepository = visitRepository;
        this.assistanceService = assistanceService;
        this.mailService = mailService;
    }

    @Override
    public List<Visit> getAll() {
        return visitRepository.getAll();
    }

    @Override
    public Visit getById(long id) {
        return visitRepository.getById(id);
    }

    @Override
    public void create(Visit visit) {
        visitRepository.create(visit);
    }

    @Override
    public void update(Visit visit) {
        if (visit.isComplete()) {
            throw new UnauthorizedOperationException("This visit is already marked complete.");
        }
        visitRepository.update(visit);
    }

    @Override
    public void delete(long id) {
        visitRepository.delete(id);
    }

    @Override
    public void addServiceToVisit(long id, Assistance assistance) {
        Visit visit = getById(id);
        if (visit.isComplete()) {
            throw new UnauthorizedOperationException("This visit is already marked complete.");
        }
        if (visit.getServices().add(assistance)) {
            update(visit);
        } else throw new DuplicateEntityException("Service", "name", assistance.getName());
    }

    @Override
    public void removeServiceFromVisit(long id, Assistance assistance) {
        Visit visit = getById(id);
        if (visit.isComplete()) {
            throw new UnauthorizedOperationException("This visit is already marked complete.");
        }
        if (visit.getServices().remove(assistance)) {
            update(visit);
        } else throw new EntityNotFoundException("Service", "name", assistance.getName());
    }

    @Override
    public void closeVisit(long id) {
        Visit visit = getById(id);
        if (visit.isComplete()) {
            throw new UnauthorizedOperationException("This visit is already marked complete.");
        }
        visit.setComplete(true);
        visitRepository.update(visit);
    }

    @Override
    public List<Visit> search(Optional<String> search) {
        return visitRepository.search(search);
    }

    @Override
    public List<Visit> filter(Optional<String> username,
                              Optional<String> brand,
                              Optional<String> model,
                              Optional<String> licensePlate,
                              Optional<String> vin,
                              Optional<String> dateFrom,
                              Optional<String> dateTo,
                              Optional<String> sort,
                              Optional<String> order) {
        return visitRepository.filter(username, brand, model, licensePlate, vin, dateFrom, dateTo, sort, order);
    }

    @Override
    public List<Visit> getUserVisits(User user) {
        return filter(Optional.of(user.getUsername()),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
    }

    @Override
    public List<Assistance> getAssistancesInCurrency(long id, String currency) {
        Visit visit = getById(id);
        List<Assistance> assistances = new ArrayList<>(visit.getServices());
        return assistanceService.getAssistancesInCurrency(assistances, currency);
    }

    @Override
    public double getTotalPrice(Visit visit) {
        double totalPrice = 0;
        Set<Assistance> assistances = visit.getServices();
        for (Assistance assistance : assistances) {
            totalPrice += assistance.getPrice();
        }
        return totalPrice;
    }

    @Override
    public double getTotalPriceInCurrency(Visit visit, String currency) {
        double priceToConvert = getTotalPrice(visit);
        double exchangeRate = CurrencyConverter.getExchangeRates(currency);
        return CurrencyConverter.convertPrice(priceToConvert, exchangeRate);
    }

    @Override
    public void sendVisitReportEmail(Visit visit) {
        String email = visit.getUser().getEmail();
        String subject = "Detailed report for visit #" + visit.getId();
        String content = getReportAsString(visit, "BGN") + EMAIL_FOOTER;
        mailService.sendEmail(email, subject, content);
    }

//    @Override
//    public void sendVisitReportEmailInCurrency(Visit visit, String currency) {
//        String email = visit.getUser().getEmail();
//        String subject = "Detailed report for visit #" + visit.getId();
//        String content = getReportAsString(visit, currency) + EMAIL_FOOTER;
//        mailService.sendEmail(email, subject, content);
//    }

    private String getReportAsString(Visit visit, String currency) {
        Set<Assistance> services = visit.getServices();
        String servicesAsString = listOfServicesAsString(services);
        double servicesTotal = getTotalPrice(visit);
        return String.format(
                "Visit #%d\n" +
                        "Car details:\n" +
                        "Brand: %s\n" +
                        "Model: %s\n" +
                        "License Plate Number: %s\n" +
                        "Registered to: %s %s\n" +
                        "\n" +
                        "List of services:\n" +
                        "%s\n" +
                        "\n" +
                        "Total price for all services: %.2f %s\n",
                visit.getId(),
                visit.getVehicle().getModel().getBrand().getName(),
                visit.getVehicle().getModel().getName(),
                visit.getVehicle().getLicensePlate(),
                visit.getUser().getFirstName(),
                visit.getUser().getLastName(),
                servicesAsString,
                servicesTotal,
                currency);
    }

    private String listOfServicesAsString(Set<Assistance> services) {
        List<Assistance> servicesList = List.copyOf(services);
        StringBuilder sb = new StringBuilder();
        servicesList.forEach(service -> sb.append(service.getName()).append(": ").append(service.getPrice()).append("\n"));
        return sb.toString();
    }
}