package com.teamit.smartgarage.services.contracts;

import com.teamit.smartgarage.models.Assistance;

import java.util.List;
import java.util.Optional;

public interface AssistanceService {

    List<Assistance> getAll();

    Assistance getById(long id);

    Assistance getByName(String name);

    //todo current user to perform create - check in service layer
    void create(Assistance assistance);

    //todo current user to perform update - check in service layer
    void update(Assistance assistance);

    //todo current user to perform delete - check in service layer
    void delete(long id);

    Assistance getByIdInCurrency(long id, String currency);

    List<Assistance> getAllInCurrency(String currency);

    List<Assistance> getAssistancesInCurrency(List<Assistance> assistances, String currency);

    List<Assistance> search(Optional<String> search);

    List<Assistance> filter(Optional<String> name,
                            Optional<Double> priceFrom,
                            Optional<Double> priceTo,
                            Optional<String> sort,
                            Optional<String> order);
}
