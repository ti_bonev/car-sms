package com.teamit.smartgarage.services;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.VehicleBrand;
import com.teamit.smartgarage.repositories.contracts.VehicleBrandRepository;
import com.teamit.smartgarage.services.contracts.VehicleBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleBrandServiceImpl implements VehicleBrandService {

    private final VehicleBrandRepository vehicleBrandRepository;

    @Autowired
    public VehicleBrandServiceImpl(VehicleBrandRepository vehicleBrandRepository) {
        this.vehicleBrandRepository = vehicleBrandRepository;
    }

    @Override
    public List<VehicleBrand> getAll() {
        return vehicleBrandRepository.getAll();
    }

    @Override
    public VehicleBrand getById(long id) {
        return vehicleBrandRepository.getById(id);
    }

    @Override
    public VehicleBrand getByName(String name) {
        return vehicleBrandRepository.getByName(name);
    }

    @Override
    public void create(VehicleBrand vehicleBrand) {
        try {
            vehicleBrandRepository.getByName(vehicleBrand.getName());
            throw new DuplicateEntityException("Brand", "name", vehicleBrand.getName());
        } catch (EntityNotFoundException e) {
            vehicleBrandRepository.create(vehicleBrand);
        }
    }

    @Override
    public void update(VehicleBrand vehicleBrand) {
        try {
            VehicleBrand existingVehicleBrand = vehicleBrandRepository.getByName(vehicleBrand.getName());
            if (existingVehicleBrand.getId() != vehicleBrand.getId()) {
                throw new DuplicateEntityException("Brand", "name", vehicleBrand.getName());
            }
        } catch (EntityNotFoundException e) {
            vehicleBrandRepository.update(vehicleBrand);
        }
    }

    @Override
    public void delete(long id) {
        vehicleBrandRepository.delete(id);
    }

    @Override
    public List<VehicleBrand> search(Optional<String> search) {
        return vehicleBrandRepository.search(search);
    }

    @Override
    public List<VehicleBrand> filter(Optional<String> sort, Optional<String> order) {
        return vehicleBrandRepository.filter(sort, order);
    }
}
