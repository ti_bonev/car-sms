package com.teamit.smartgarage.utils.mappers;

import com.teamit.smartgarage.models.VehicleBrand;
import com.teamit.smartgarage.models.dtos.VehicleBrandDTO;
import com.teamit.smartgarage.services.contracts.VehicleBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VehicleBrandMapper {

    private final VehicleBrandService vehicleBrandService;

    @Autowired
    public VehicleBrandMapper(VehicleBrandService vehicleBrandService) {
        this.vehicleBrandService = vehicleBrandService;
    }

    public VehicleBrand fromDTO(VehicleBrandDTO vehicleBrandDTO) {
        VehicleBrand vehicleBrand = new VehicleBrand();
        dtoToObject(vehicleBrandDTO, vehicleBrand);
        return vehicleBrand;
    }

    public VehicleBrand fromDTO(long id, VehicleBrandDTO vehicleBrandDTO) {
        VehicleBrand vehicleBrandToUpdate = vehicleBrandService.getById(id);
        dtoToObject(vehicleBrandDTO, vehicleBrandToUpdate);
        return vehicleBrandToUpdate;
    }

    private void dtoToObject(VehicleBrandDTO vehicleBrandDTO, VehicleBrand vehicleBrand) {
        vehicleBrand.setName(vehicleBrandDTO.getName());
    }
}
