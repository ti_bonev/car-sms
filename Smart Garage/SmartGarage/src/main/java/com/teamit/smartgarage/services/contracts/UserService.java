package com.teamit.smartgarage.services.contracts;

import com.teamit.smartgarage.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAll();

    void sendNewPasswordEmail(User user);

    List<User> search(Optional<String> search);

    User getByID(long id);

    User getByUsername(String username);

    User getByEmail(String email);

    User getByPhone(String phone);

    void create(User user);

    void update(User userToUpdate, User currentUser);

    void delete(long id, User currentUser);

    boolean isClient(User user);

    boolean isEmployee(User user);

    boolean isAdmin(User user);

    List<User> filter(Optional<String> firstName,
                      Optional<String> lastName,
                      Optional<String> role,
                      Optional<String> model,
                      Optional<String> brand,
                      Optional<String> sort,
                      Optional<String> order);

    User getAdmin();
}