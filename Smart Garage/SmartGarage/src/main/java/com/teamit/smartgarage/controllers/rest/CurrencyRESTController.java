package com.teamit.smartgarage.controllers.rest;

import com.teamit.smartgarage.exceptions.CurrencyConversionException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.Assistance;
import com.teamit.smartgarage.models.Visit;
import com.teamit.smartgarage.services.contracts.AssistanceService;
import com.teamit.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/garage/currency")
public class CurrencyRESTController {

    private final AssistanceService assistanceService;
    private final VisitService visitService;

    @Autowired
    public CurrencyRESTController(AssistanceService assistanceService, VisitService visitService) {
        this.assistanceService = assistanceService;
        this.visitService = visitService;
    }

    @GetMapping("/{id}/{currencySignature}")
    public Assistance getByIdInCurrency(@PathVariable long id, @PathVariable String currencySignature) {
        try {
            return assistanceService.getByIdInCurrency(id, currencySignature);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (CurrencyConversionException e) {
            throw new ResponseStatusException(HttpStatus.REQUEST_TIMEOUT, e.getMessage());
        }
    }

    @GetMapping("/{currencySignature}")
    public List<Assistance> getAllInCurrency(@PathVariable String currencySignature) {
        try {
            return assistanceService.getAllInCurrency(currencySignature);
        } catch (CurrencyConversionException e) {
            throw new ResponseStatusException(HttpStatus.REQUEST_TIMEOUT, e.getMessage());
        }
    }

    @GetMapping("/visit/{id}/{currency}")
    public List<Assistance> getAssistancesInCurrency(@PathVariable long id, @PathVariable String currency) {
        try {
            return visitService.getAssistancesInCurrency(id, currency);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (CurrencyConversionException e) {
            throw new ResponseStatusException(HttpStatus.REQUEST_TIMEOUT, e.getMessage());
        }
    }

    @GetMapping("/visits/{id}/total/{currency}")
    public double getTotalPriceInCurrency(@PathVariable int id, @PathVariable String currency) {
        try {
            Visit visit = visitService.getById(id);
            return visitService.getTotalPriceInCurrency(visit, currency);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (CurrencyConversionException e) {
            throw new ResponseStatusException(HttpStatus.REQUEST_TIMEOUT, e.getMessage());
        }
    }
}