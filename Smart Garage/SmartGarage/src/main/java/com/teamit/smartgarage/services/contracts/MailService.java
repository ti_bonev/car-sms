package com.teamit.smartgarage.services.contracts;

public interface MailService {

    void sendEmail(String email, String subject, String content);

}