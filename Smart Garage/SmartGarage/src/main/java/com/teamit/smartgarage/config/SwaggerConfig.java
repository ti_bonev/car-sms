package com.teamit.smartgarage.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Once the project is running you can access the Swagger documentation
     * with endpoint "/swagger-ui/"
     * or at: http://localhost:8080/swagger-ui/
     */

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.teamit.smartgarage"))
                .build()
                .apiInfo(apiDetails());
    }

    private ApiInfo apiDetails() {
        return new ApiInfo(
                "Smart garage",
                "Car service management system",
                "1.0",
                "Open-source software",
                new springfox.documentation.service.Contact(
                        "Mighty IT team - Ivo & Tosho",
                        "https://gitlab.com/ti_bonev/car-sms.git",
                        "mighty_it@abv.bg"),
                "Telerik Academy",
                "https://www.telerikacademy.com/",
                Collections.emptyList());
    }
}