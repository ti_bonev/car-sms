package com.teamit.smartgarage.services.contracts;

import com.teamit.smartgarage.models.VehicleModel;

import java.util.List;
import java.util.Optional;

public interface VehicleModelService {

    List<VehicleModel> getAll();

    VehicleModel getById(long id);

    VehicleModel getByName(String name);

    void create(VehicleModel vehicleModel);

    void update(VehicleModel vehicleModel);

    void delete(long id);

    List<VehicleModel> search(Optional<String> search);

    List<VehicleModel> filter(Optional<String> brand, Optional<String> sort, Optional<String> order);
}
