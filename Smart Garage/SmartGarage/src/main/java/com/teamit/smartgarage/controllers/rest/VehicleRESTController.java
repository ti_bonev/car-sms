package com.teamit.smartgarage.controllers.rest;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.Vehicle;
import com.teamit.smartgarage.models.dtos.VehicleDTO;
import com.teamit.smartgarage.services.contracts.VehicleService;
import com.teamit.smartgarage.utils.mappers.VehicleMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/garage/vehicles")
public class VehicleRESTController {

    private final VehicleService vehicleService;
    private final VehicleMapper vehicleMapper;

    public VehicleRESTController(VehicleService vehicleService, VehicleMapper vehicleMapper) {
        this.vehicleService = vehicleService;
        this.vehicleMapper = vehicleMapper;
    }

    @GetMapping
    public List<Vehicle> getAll(@RequestParam(required = false) Optional<String> search) {
        if (search.isPresent()) {
            return vehicleService.search(search);
        }
        return vehicleService.getAll();
    }

    @GetMapping("/filter")
    public List<Vehicle> filter(@RequestParam(required = false) Optional<String> username,
                                @RequestParam(required = false) Optional<String> brand,
                                @RequestParam(required = false) Optional<String> model,
                                @RequestParam(required = false) Optional<String> sort,
                                @RequestParam(required = false) Optional<String> order) {
        try {
            return vehicleService.filter(username, brand, model, sort, order);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Vehicle getById(@PathVariable long id) {
        try {
            return vehicleService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void create(@Valid @RequestBody VehicleDTO vehicleDTO) {
        try {
            Vehicle vehicle = vehicleMapper.fromDTO(vehicleDTO);
            vehicleService.create(vehicle);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@PathVariable long id, @Valid @RequestBody VehicleDTO vehicleDTO) {
        try {
            Vehicle vehicleToUpdate = vehicleMapper.fromDTO(id, vehicleDTO);
            vehicleService.update(vehicleToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        try {
            vehicleService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
