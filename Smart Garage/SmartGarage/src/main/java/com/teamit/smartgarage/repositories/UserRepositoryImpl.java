package com.teamit.smartgarage.repositories;

import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl
        extends AbstractCRUDRepository<User>
        implements UserRepository {

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }

    @Override
    public List<User> search(Optional<String> search) {
        try (Session session = sessionFactory.openSession()) {
            if (search.isEmpty())
                return getAll();
            Query<User> query = session.createQuery(" from User where " +
                            "username like :search or " +
                            "email like :search or " +
                            "phone like :search or " +
                            "firstName like :search or " +
                            "lastName like :search or " +
                            "role.roleName like :search",
                    User.class);
            query.setParameter("search", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<User> filter(Optional<String> firstName,
                             Optional<String> lastName,
                             Optional<String> role,
                             Optional<String> model,
                             Optional<String> brand,
                             Optional<String> sort,
                             Optional<String> order) {
        try (Session session = sessionFactory.openSession()) {
            var queryBuild = new StringBuilder(" from User ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();
            firstName.ifPresent(value -> {
                filter.add(" firstName like :firstName ");
                queryParams.put("firstName", "%" + value + "%");
            });
            lastName.ifPresent(value -> {
                filter.add(" lastName like :lastName ");
                queryParams.put("lastName", "%" + value + "%");
            });
            role.ifPresent(value -> {
                filter.add(" role.roleName like :role ");
                queryParams.put("role", "%" + value + "%");
            });
            //todo implement filter by model, brand

            if (!filter.isEmpty()) {
                queryBuild.append(" where ").append(String.join(" and ", filter));
            }
            sort.ifPresent(value -> queryBuild.append(sortBy(value, order)));
            Query<User> query = session.createQuery(queryBuild.toString(), User.class);
            query.setProperties(queryParams);
            System.out.println(queryBuild);
            return query.list();
        }
    }

    private String sortBy(String value, Optional<String> order) {
        var queryBuild = new StringBuilder(" order by ");
        switch (value) {
            case "firstName":
                queryBuild.append(" firstName ");
                break;
            case "lastName":
                queryBuild.append(" lastName ");
                break;
            case "role":
                queryBuild.append(" role.roleName ");
                break;
            default:
                throw new UnsupportedOperationException(
                        "Type mismatch. Choose 'firstName', 'lastName' or 'role'. ");
        }
        order.ifPresent(sortOrder -> {
            switch (sortOrder) {
                case "ascending":
                    queryBuild.append(" asc ");
                    break;
                case "descending":
                    queryBuild.append(" desc ");
                    break;
                default:
                    throw new UnsupportedOperationException(
                            "Type mismatch. Choose 'ascending' or 'descending'."
                    );
            }
        });
        return queryBuild.toString();
    }
}