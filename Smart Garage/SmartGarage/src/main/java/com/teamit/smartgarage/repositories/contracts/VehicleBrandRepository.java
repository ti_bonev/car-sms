package com.teamit.smartgarage.repositories.contracts;

import com.teamit.smartgarage.models.VehicleBrand;

import java.util.List;
import java.util.Optional;

public interface VehicleBrandRepository extends BaseCRUDRepository<VehicleBrand> {

    VehicleBrand getByName(String name);

    List<VehicleBrand> search(Optional<String> search);

    List<VehicleBrand> filter(Optional<String> sort, Optional<String> order);
}
