package com.teamit.smartgarage.models.dtos;

import com.teamit.smartgarage.models.Visit;

public class VisitDTOOut extends Visit {

    private double totalPrice;
    private String currency;

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}