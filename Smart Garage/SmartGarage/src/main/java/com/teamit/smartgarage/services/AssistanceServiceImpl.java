package com.teamit.smartgarage.services;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.Assistance;
import com.teamit.smartgarage.repositories.contracts.AssistanceRepository;
import com.teamit.smartgarage.services.contracts.AssistanceService;
import com.teamit.smartgarage.utils.currencyconverter.CurrencyConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AssistanceServiceImpl implements AssistanceService {

    public static final String DELETED = "[No longer supported]";
    private final AssistanceRepository assistanceRepository;

    @Autowired
    public AssistanceServiceImpl(AssistanceRepository assistanceRepository) {
        this.assistanceRepository = assistanceRepository;
    }

    @Override
    public List<Assistance> getAll() {
        List<Assistance> assistances = assistanceRepository.getAll();
        removeDeletedAssistances(assistances);
        return assistances;
    }

    @Override
    public Assistance getById(long id) {
        return assistanceRepository.getById(id);
    }

    @Override
    public Assistance getByIdInCurrency(long id, String currency) {
        Assistance assistance = assistanceRepository.getById(id);
        double exchangeRate = CurrencyConverter.getExchangeRates(currency);
        assistance.setPrice(CurrencyConverter.convertPrice(assistance.getPrice(), exchangeRate));
        return assistance;
    }

    @Override
    public List<Assistance> getAllInCurrency(String currency) {
        List<Assistance> assistances = assistanceRepository.getAll();
        removeDeletedAssistances(assistances);
        double exchangeRate = CurrencyConverter.getExchangeRates(currency);
        for (Assistance assistance : assistances) {
            assistance.setPrice(CurrencyConverter.convertPrice(assistance.getPrice(), exchangeRate));
        }
        return assistances;
    }

    @Override
    public List<Assistance> getAssistancesInCurrency(List<Assistance> assistances, String currency) {
        double exchangeRate = CurrencyConverter.getExchangeRates(currency);
        for (Assistance assistance : assistances) {
            assistance.setPrice(CurrencyConverter.convertPrice(assistance.getPrice(), exchangeRate));
        }
        //todo format prices 0.00
        return assistances;
    }

    @Override
    public Assistance getByName(String name) {
        return assistanceRepository.getByName(name);
    }

    @Override
    public void create(Assistance assistance) {
        try {
            assistanceRepository.getByName(assistance.getName());
            throw new DuplicateEntityException("Service", "name", assistance.getName());
        } catch (EntityNotFoundException e) {
            assistanceRepository.create(assistance);
        }
    }

    @Override
    public void update(Assistance assistance) {
        boolean hasDuplicate = true;
        try {
            Assistance existingAssistance = assistanceRepository.getByName(assistance.getName());
            if (existingAssistance.getId() == assistance.getId()) {
                hasDuplicate = false;
            }
        } catch (EntityNotFoundException e) {
            hasDuplicate = false;
        }
        if (hasDuplicate) {
            throw new DuplicateEntityException("Service", "name", assistance.getName());
        }
        assistanceRepository.update(assistance);
    }

    //TODO OPTIMIZATION - rename method to preserve original CRUD delete method.
    @Override
    public void delete(long id) {
        Assistance assistanceToDelete = getById(id);
        if (assistanceToDelete.getName().contains(DELETED)) {
            throw new EntityNotFoundException("Service", id);
        }
        assistanceToDelete.setName(DELETED + "[" + assistanceToDelete.getId() + "] " + assistanceToDelete.getName());
        update(assistanceToDelete);
    }

//    Original CRUD delete method;
//
//    @Override
//    public void delete(long id) {
//        assistanceRepository.delete(id);
//    }

    @Override
    public List<Assistance> search(Optional<String> search) {
        return assistanceRepository.search(search);
    }

    @Override
    public List<Assistance> filter(Optional<String> name, Optional<Double> priceFrom, Optional<Double> priceTo, Optional<String> sort, Optional<String> order) {
        return assistanceRepository.filter(name, priceFrom, priceTo, sort, order);
    }

    private void removeDeletedAssistances(List<Assistance> assistances) {
        assistances.removeIf(assistance -> assistance.getName().contains(DELETED));
    }
}