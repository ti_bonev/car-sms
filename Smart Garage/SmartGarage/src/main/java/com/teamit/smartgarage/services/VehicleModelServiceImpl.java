package com.teamit.smartgarage.services;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.VehicleModel;
import com.teamit.smartgarage.repositories.contracts.VehicleModelRepository;
import com.teamit.smartgarage.services.contracts.VehicleModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleModelServiceImpl implements VehicleModelService {

    private final VehicleModelRepository vehicleModelRepository;

    @Autowired
    public VehicleModelServiceImpl(VehicleModelRepository vehicleModelRepository) {
        this.vehicleModelRepository = vehicleModelRepository;
    }

    @Override
    public List<VehicleModel> getAll() {
        return vehicleModelRepository.getAll();
    }

    @Override
    public VehicleModel getById(long id) {
        return vehicleModelRepository.getById(id);
    }

    @Override
    public VehicleModel getByName(String name) {
        return vehicleModelRepository.getByName(name);
    }

    @Override
    public void create(VehicleModel vehicleModel) {
        try {
            vehicleModelRepository.getByName(vehicleModel.getName());
            throw new DuplicateEntityException("Model", "name", vehicleModel.getName());
        } catch (EntityNotFoundException e) {
            vehicleModelRepository.create(vehicleModel);
        }
    }

    @Override
    public void update(VehicleModel vehicleModel) {
        try {
            VehicleModel existingVehicleModel = vehicleModelRepository.getByName(vehicleModel.getName());
            if (existingVehicleModel.getId() != vehicleModel.getId()) {
                throw new DuplicateEntityException("Model", "name", vehicleModel.getName());
            }
        } catch (EntityNotFoundException ignored) {
        }
        vehicleModelRepository.update(vehicleModel);
    }

    @Override
    public void delete(long id) {
        vehicleModelRepository.delete(id);
    }

    @Override
    public List<VehicleModel> search(Optional<String> search) {
        return vehicleModelRepository.search(search);
    }

    @Override
    public List<VehicleModel> filter(Optional<String> brand, Optional<String> sort, Optional<String> order) {
        return vehicleModelRepository.filter(brand, sort, order);
    }
}
