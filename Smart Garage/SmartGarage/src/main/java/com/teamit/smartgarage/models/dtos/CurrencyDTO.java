package com.teamit.smartgarage.models.dtos;

public class CurrencyDTO {
    private String currency;

    public CurrencyDTO() {
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}