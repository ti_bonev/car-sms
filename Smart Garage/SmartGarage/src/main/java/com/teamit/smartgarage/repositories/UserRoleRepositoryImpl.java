package com.teamit.smartgarage.repositories;

import com.teamit.smartgarage.models.UserRole;
import com.teamit.smartgarage.repositories.contracts.UserRoleRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRoleRepositoryImpl
        extends AbstractReadRepository<UserRole>
        implements UserRoleRepository {

    @Autowired
    public UserRoleRepositoryImpl(SessionFactory sessionFactory) {
        super(UserRole.class, sessionFactory);
    }
}
