package com.teamit.smartgarage.models.dtos;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class VehicleBaseDTO {

    @Size(min = 7, max = 8, message = "Vehicle plate number should be a valid bulgarian plate number.")
    private String licensePlate;

    @Size(min = 17, max = 17, message = "Must be valid VIN number.")
    private String vin;

    @Min(value = 1886)
    private int productionYear;

    public VehicleBaseDTO() {
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(int productionYear) {
        this.productionYear = productionYear;
    }
}