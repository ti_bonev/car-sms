package com.teamit.smartgarage.controllers.mvc;

import com.teamit.smartgarage.models.VehicleBrand;
import com.teamit.smartgarage.models.VehicleModel;
import com.teamit.smartgarage.models.dtos.FilterVehicleModelDTO;
import com.teamit.smartgarage.models.dtos.SearchDTO;
import com.teamit.smartgarage.services.contracts.VehicleBrandService;
import com.teamit.smartgarage.services.contracts.VehicleModelService;
import com.teamit.smartgarage.utils.mappers.VehicleModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/v1/models")
public class VehicleModelMVCController {

    private final VehicleModelService vehicleModelService;
    private final VehicleModelMapper vehicleModelMapper;
    private final VehicleBrandService vehicleBrandService;

    @Autowired
    public VehicleModelMVCController(VehicleModelService vehicleModelService, VehicleModelMapper vehicleModelMapper, VehicleBrandService vehicleBrandService) {
        this.vehicleModelService = vehicleModelService;
        this.vehicleModelMapper = vehicleModelMapper;
        this.vehicleBrandService = vehicleBrandService;
    }

    @ModelAttribute("models")
    public List<VehicleModel> populateModels() {
        return vehicleModelService.getAll();
    }

    @ModelAttribute("brands")
    public List<VehicleBrand> populateBrands() {
        return vehicleBrandService.getAll();
    }

    //TODO add sort model attribute

    @ModelAttribute("search")
    public SearchDTO populateSearch() {
        return new SearchDTO();
    }

    @ModelAttribute("filter")
    public FilterVehicleModelDTO populateFilter() {
        return new FilterVehicleModelDTO();
    }

    @ModelAttribute("searchEndPoint")
    public String populateSearchEndPoint() {
        return "/v1/models/search";
    }

    @ModelAttribute("filterEndPoint")
    public String populateFilterEndPoint() {
        return "/v1/models/filter";
    }

    @GetMapping
    public String showModelsPage(Model model) {
        model.addAttribute("vehicleModels", vehicleModelService.getAll());
        return "vehicle-model-all";
    }

    @PostMapping("/search")
    public String searchModels(@Valid @ModelAttribute("search") SearchDTO searchDTO,
                               BindingResult bindingResult,
                               Model model) {
        if (bindingResult.hasErrors()) {
            return "vehicle-model-all";
        }
        var searchResult = vehicleModelService.search(
                Optional.ofNullable(searchDTO.getKeyword().isBlank() ? null : searchDTO.getKeyword())
        );
        model.addAttribute("vehicleModels", searchResult);
        return "vehicle-model-all";
    }
}
