package com.teamit.smartgarage.services;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.Vehicle;
import com.teamit.smartgarage.repositories.contracts.VehicleRepository;
import com.teamit.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public List<Vehicle> getAll() {
        return vehicleRepository.getAll();
    }

    @Override
    public Vehicle getById(long id) {
        return vehicleRepository.getById(id);
    }

    @Override
    public void create(Vehicle vehicle) {
        boolean hasDuplicate = true;
        try {
            vehicleRepository.getByField("licensePlate", vehicle.getLicensePlate());
        } catch (EntityNotFoundException e) {
            hasDuplicate = false;
        }
        if (hasDuplicate) {
            throw new DuplicateEntityException("Vehicle", "license plate", vehicle.getLicensePlate());
        }
        hasDuplicate = true;
        try {
            vehicleRepository.getByField("vin", vehicle.getVin());
        } catch (EntityNotFoundException e) {
            hasDuplicate = false;
        }
        if (hasDuplicate) {
            throw new DuplicateEntityException("Vehicle", "VIN", vehicle.getVin());
        }
        vehicleRepository.create(vehicle);
    }

    @Override
    public void update(Vehicle vehicle) {
        boolean hasDuplicate = true;
        try {
            Vehicle existingVehicle = vehicleRepository.getByField("licensePlate", vehicle.getLicensePlate());
            if (existingVehicle.getId() == vehicle.getId()) {
                hasDuplicate = false;
            }
        } catch (EntityNotFoundException e) {
            hasDuplicate = false;
        }
        if (hasDuplicate) {
            throw new DuplicateEntityException("Vehicle", "license plate", vehicle.getLicensePlate());
        }
        hasDuplicate = true;
        try {
            Vehicle existingVehicle = vehicleRepository.getByField("vin", vehicle.getVin());
            if (existingVehicle.getId() == vehicle.getId()) {
                hasDuplicate = false;
            }
        } catch (EntityNotFoundException e) {
            hasDuplicate = false;
        }
        if (hasDuplicate) {
            throw new DuplicateEntityException("Vehicle", "VIN", vehicle.getVin());
        }
        vehicleRepository.update(vehicle);
    }

    @Override
    public void delete(long id) {
        vehicleRepository.delete(id);
    }

    @Override
    public List<Vehicle> search(Optional<String> search) {
        return vehicleRepository.search(search);
    }

    @Override
    public List<Vehicle> filter(Optional<String> username, Optional<String> brand, Optional<String> model, Optional<String> sort, Optional<String> order) {
        return vehicleRepository.filter(username, brand, model, sort, order);
    }

    @Override
    public List<Vehicle> getUserVehicles(User user) {
        return filter(Optional.of(user.getUsername()),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
    }
}