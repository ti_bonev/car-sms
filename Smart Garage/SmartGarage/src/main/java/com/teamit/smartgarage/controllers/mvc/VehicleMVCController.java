package com.teamit.smartgarage.controllers.mvc;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.Vehicle;
import com.teamit.smartgarage.models.VehicleBrand;
import com.teamit.smartgarage.models.VehicleModel;
import com.teamit.smartgarage.models.dtos.EntityIdDTO;
import com.teamit.smartgarage.models.dtos.FilterVehicleDTO;
import com.teamit.smartgarage.models.dtos.SearchDTO;
import com.teamit.smartgarage.models.dtos.VehicleBaseDTO;
import com.teamit.smartgarage.services.contracts.UserService;
import com.teamit.smartgarage.services.contracts.VehicleBrandService;
import com.teamit.smartgarage.services.contracts.VehicleModelService;
import com.teamit.smartgarage.services.contracts.VehicleService;
import com.teamit.smartgarage.utils.mappers.VehicleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/v1/vehicles")
public class VehicleMVCController {

    private final VehicleService vehicleService;
    private final VehicleMapper vehicleMapper;
    private final VehicleModelService vehicleModelService;
    private final VehicleBrandService vehicleBrandService;
    private final UserService userService;

    @Autowired
    public VehicleMVCController(VehicleService vehicleService,
                                VehicleMapper vehicleMapper,
                                VehicleModelService vehicleModelService,
                                VehicleBrandService vehicleBrandService, UserService userService) {
        this.vehicleService = vehicleService;
        this.vehicleMapper = vehicleMapper;
        this.vehicleModelService = vehicleModelService;
        this.vehicleBrandService = vehicleBrandService;
        this.userService = userService;
    }

    @ModelAttribute("searchEndPoint")
    public String populateSearchEndPoint() {
        return "/v1/vehicles/search";
    }

    //todo link to external models list
    @ModelAttribute("vehicleModels")
    public List<VehicleModel> populateModels() {
        return vehicleModelService.getAll();
    }

    //todo link to external brands list
    @ModelAttribute("vehicleBrands")
    public List<VehicleBrand> populateBrands() {
        return vehicleBrandService.getAll();
    }

    @ModelAttribute("filter")
    public FilterVehicleDTO populateFilter() {
        return new FilterVehicleDTO();
    }

    @GetMapping
    public String showVehiclesPage(Model model) {
        model.addAttribute("vehicles", vehicleService.getAll());
        return "vehicle-all";
    }

    @GetMapping("/{id}")
    public String showVehiclePage(@PathVariable long id,
                                  Model model) {
        try {
            Vehicle vehicle = vehicleService.getById(id);
            model.addAttribute("vehicle", vehicle);
            return "vehicle";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new/{id}")
    public String showCreateVehiclePage(@PathVariable long id,
                                        Model model) {
        model.addAttribute("vehicle", new VehicleBaseDTO());
        model.addAttribute("vehicleModel", new EntityIdDTO());
        return "vehicle-new";
    }

    @PostMapping("/new/{id}")
    public String createVehicle(@PathVariable long id,
                                @Valid @ModelAttribute("vehicle") VehicleBaseDTO vehicleBaseDTO,
                                BindingResult errors,
                                @Valid @ModelAttribute("vehicleModel") EntityIdDTO entityIdDTO,
                                Model model) {
        if (errors.hasErrors())
            return "vehicle-new";
        try {
            User owner = userService.getByID(id);
            long vehicleModelID = entityIdDTO.getId();
            Vehicle vehicle = vehicleMapper.fromDTO(vehicleBaseDTO, vehicleModelID, owner);
            vehicleService.create(vehicle);
            return "redirect:/v1/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("licensePlate", "duplicate.licensePlate", e.getMessage());
            errors.rejectValue("vin", "duplicate.vin", e.getMessage());
            return "vehicle-new";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdateVehiclePage(@PathVariable long id,
                                        Model model) {
        try {
            Vehicle vehicle = vehicleService.getById(id);
            VehicleBaseDTO vehicleBaseDTO = vehicleMapper.toDTO(vehicle);
            model.addAttribute("vehicle", vehicleBaseDTO);
            model.addAttribute("vehicleModel", new EntityIdDTO());
            return "vehicle-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateVehicle(@PathVariable long id,
                                @Valid @ModelAttribute("vehicle") VehicleBaseDTO vehicleBaseDTO,
                                BindingResult errors,
                                @Valid @ModelAttribute("vehicleModel") EntityIdDTO entityIdDTO,
                                Model model) {
        if (errors.hasErrors())
            return "vehicle-new";
        try {
            long vehicleModelID = entityIdDTO.getId();
            Vehicle vehicle = vehicleMapper.fromDTO(id, vehicleBaseDTO, vehicleModelID);
            vehicleService.update(vehicle);
            return "redirect:/v1/vehicles/" + id;
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "vehicle-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/search")
    public String searchVehicle(@Valid @ModelAttribute("search") SearchDTO searchDTO,
                                BindingResult bindingResult,
                                Model model) {
        if (bindingResult.hasErrors())
            return "vehicle-all";
        var searchResult = vehicleService.search(
                Optional.ofNullable(searchDTO.getKeyword().isBlank() ? null : searchDTO.getKeyword())
        );
        model.addAttribute("vehicles", searchResult);
        return "vehicle-all";
    }

    @PostMapping("/filter")
    public String filterVehicles(@Valid @ModelAttribute("filter") FilterVehicleDTO filterVehicleDTO,
                                 BindingResult bindingResult,
                                 Model model) {
        if (bindingResult.hasErrors())
            return "vehicle-all";
        var filterResult = vehicleService.filter(
                Optional.ofNullable(filterVehicleDTO.getUsername().isBlank() ? null : filterVehicleDTO.getUsername()),
                Optional.ofNullable(filterVehicleDTO.getBrand().isBlank() ? null : filterVehicleDTO.getBrand()),
                Optional.ofNullable(filterVehicleDTO.getModel().isBlank() ? null : filterVehicleDTO.getModel()),
                Optional.ofNullable(filterVehicleDTO.getSort().isBlank() ? null : filterVehicleDTO.getSort()),
                Optional.ofNullable(filterVehicleDTO.getOrder().isBlank() ? null : filterVehicleDTO.getOrder())
        );
        model.addAttribute("vehicles", filterResult);
        return "vehicle-all";
    }

    @GetMapping("/{id}/delete")
    public String deleteVehicle(@PathVariable long id,
                                Model model) {
        //todo soft delete
        try {
            vehicleService.delete(id);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        }
        return "redirect:/v1";
    }
}