package com.teamit.smartgarage.controllers.rest;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.exceptions.UnauthorizedOperationException;
import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.dtos.UserDTO;
import com.teamit.smartgarage.services.contracts.UserService;
import com.teamit.smartgarage.utils.helpers.AuthenticationHelper;
import com.teamit.smartgarage.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/garage/users")
public class UserRESTController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserRESTController(UserService userService,
                              UserMapper userMapper,
                              AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) Optional<String> search) {
        authenticationHelper.tryGetUser(headers);
        if (search.isPresent()) return userService.search(search);
        return userService.getAll();
    }

    @GetMapping("/filter")
    public List<User> filter(@RequestParam(required = false) Optional<String> firstName,
                             @RequestParam(required = false) Optional<String> lastName,
                             @RequestParam(required = false) Optional<String> role,
// username, e-mail, phone are unique in the system and doesn't make sense to filter them
                             @RequestParam(required = false) Optional<String> model,
                             @RequestParam(required = false) Optional<String> brand,
// filter by visit will be with dto (dateStart, dateEnd) in mvc
                             @RequestParam(required = false) Optional<String> sort,
                             @RequestParam(required = false) Optional<String> order
    ) {
        try {
            return userService.filter(firstName, lastName, role, model, brand, sort, order);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public User getByID(@RequestHeader HttpHeaders headers,
                        @PathVariable long id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return userService.getByID(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void create(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody UserDTO userDTO) {
        authenticationHelper.tryGetUser(headers);
        try {

            User user = userMapper.fromDTO(userDTO);
            userService.create(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers,
                       @PathVariable long id,
                       @Valid @RequestBody UserDTO userDTO) {
        User currentUser = authenticationHelper.tryGetUser(headers);
        try {
            User userToUpdate = userMapper.fromDTO(id, userDTO);
            userService.update(userToUpdate, currentUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable long id) {
        User currentUser = authenticationHelper.tryGetUser(headers);
        try {
            userService.delete(id, currentUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
