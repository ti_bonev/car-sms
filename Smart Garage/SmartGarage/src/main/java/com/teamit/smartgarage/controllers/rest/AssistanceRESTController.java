package com.teamit.smartgarage.controllers.rest;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.Assistance;
import com.teamit.smartgarage.models.dtos.AssistanceDTO;
import com.teamit.smartgarage.services.contracts.AssistanceService;
import com.teamit.smartgarage.utils.mappers.AssistanceMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/garage/services")
public class AssistanceRESTController {

    private final AssistanceService assistanceService;
    private final AssistanceMapper assistanceMapper;

    public AssistanceRESTController(AssistanceService assistanceService, AssistanceMapper assistanceMapper) {
        this.assistanceService = assistanceService;
        this.assistanceMapper = assistanceMapper;
    }

    @GetMapping
    public List<Assistance> getAll(@RequestParam(required = false) Optional<String> search) {
        if (search.isPresent()) {
            return assistanceService.search(search);
        }
        return assistanceService.getAll();
    }

    @GetMapping("/filter")
    public List<Assistance> filter(@RequestParam(required = false) Optional<String> name,
                                   @RequestParam(required = false) Optional<Double> priceFrom,
                                   @RequestParam(required = false) Optional<Double> priceTo,
                                   @RequestParam(required = false) Optional<String> sort,
                                   @RequestParam(required = false) Optional<String> order) {
        try {
            return assistanceService.filter(name, priceFrom, priceTo, sort, order);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Assistance getById(@PathVariable long id) {
        try {
            return assistanceService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void create(@Valid @RequestBody AssistanceDTO assistanceDTO) {
        try {
            Assistance assistance = assistanceMapper.fromDTO(assistanceDTO);
            assistanceService.create(assistance);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@PathVariable long id, @Valid @RequestBody AssistanceDTO assistanceDTO) {
        try {
            Assistance assistanceToUpdate = assistanceMapper.fromDTO(id, assistanceDTO);
            assistanceService.update(assistanceToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        try {
            assistanceService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
