package com.teamit.smartgarage.repositories.contracts;

import com.teamit.smartgarage.models.Assistance;

import java.util.List;
import java.util.Optional;

public interface AssistanceRepository extends BaseCRUDRepository<Assistance> {

    Assistance getByName(String name);

    List<Assistance> search(Optional<String> search);

    List<Assistance> filter(Optional<String> name, Optional<Double> priceFrom, Optional<Double> priceTo, Optional<String> sort, Optional<String> order);
}
