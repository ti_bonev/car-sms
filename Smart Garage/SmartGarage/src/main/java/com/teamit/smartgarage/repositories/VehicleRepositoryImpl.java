package com.teamit.smartgarage.repositories;

import com.teamit.smartgarage.models.Vehicle;
import com.teamit.smartgarage.repositories.contracts.VehicleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class VehicleRepositoryImpl extends AbstractCRUDRepository<Vehicle> implements VehicleRepository {

    @Autowired
    public VehicleRepositoryImpl(SessionFactory sessionFactory) {
        super(Vehicle.class, sessionFactory);
    }

    @Override
    public List<Vehicle> search(Optional<String> search) {
        try (Session session = sessionFactory.openSession()) {
            if (search.isEmpty()) {
                return getAll();
            }
            Query<Vehicle> query = session.createQuery(" from Vehicle where " +
                    " licensePlate like :search or " +
                    " vin like :search or " +
                    " owner.username like :search or" +
                    " owner.firstName like :search or " +
                    " owner.lastName like :search or " +
                    " owner.phone like :search or " +
                    " vehicleModel.name like :search or " +
                    " vehicleModel.vehicleBrand.name like :search ", Vehicle.class);
            query.setParameter("search", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<Vehicle> filter(Optional<String> username, Optional<String> brand, Optional<String> model, Optional<String> sort, Optional<String> order) {
        try (Session session = sessionFactory.openSession()) {
            var queryBuild = new StringBuilder(" from Vehicle ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();
            username.ifPresent(value -> {
                filter.add(" owner.username like :username ");
                queryParams.put("username", "%" + value + "%");
            });
            brand.ifPresent(value -> {
                filter.add(" vehicleModel.vehicleBrand.name like :brand ");
                queryParams.put("brand", "%" + value + "%");
            });
            model.ifPresent(value -> {
                filter.add(" vehicleModel.name like :model ");
                queryParams.put("model", "%" + value + "%");
            });
            if (!filter.isEmpty()) {
                queryBuild.append(" where ").append(String.join(" and ", filter));
            }
            sort.ifPresent(value -> queryBuild.append(sortBy(value, order)));
            Query<Vehicle> query = session.createQuery(queryBuild.toString(), Vehicle.class);
            query.setProperties(queryParams);
            return query.list();
        }
    }

    private String sortBy(String value, Optional<String> order) {
        var queryBuild = new StringBuilder(" order by ");
        switch (value) {
            case "model":
                queryBuild.append(" vehicleModel.name ");
                break;
            case "brand":
                queryBuild.append(" vehicleModel.vehicleBrand.name ");
                break;
            default:
                throw new UnsupportedOperationException("Type mismatch. Choose 'brand' or 'model'.");
        }
        order.ifPresent(sortOrder -> {
            switch (sortOrder) {
                case "ascending":
                    queryBuild.append(" asc ");
                    break;
                case "descending":
                    queryBuild.append(" desc ");
                    break;
                default:
                    throw new UnsupportedOperationException(
                            "Type mismatch. Choose 'ascending' or 'descending'."
                    );
            }
        });
        return queryBuild.toString();
    }
}