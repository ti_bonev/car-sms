package com.teamit.smartgarage.controllers.mvc;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.exceptions.IncorrectEntryException;
import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.UserRole;
import com.teamit.smartgarage.models.Vehicle;
import com.teamit.smartgarage.models.Visit;
import com.teamit.smartgarage.models.dtos.FilterUserDTO;
import com.teamit.smartgarage.models.dtos.PasswordDTO;
import com.teamit.smartgarage.models.dtos.SearchDTO;
import com.teamit.smartgarage.models.dtos.UserDTO;
import com.teamit.smartgarage.services.contracts.UserRoleService;
import com.teamit.smartgarage.services.contracts.UserService;
import com.teamit.smartgarage.services.contracts.VehicleService;
import com.teamit.smartgarage.services.contracts.VisitService;
import com.teamit.smartgarage.utils.enums.UserSortOptions;
import com.teamit.smartgarage.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/v1/users")
public class UserMVCController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final UserRoleService userRoleService;
    private final VehicleService vehicleService;
    private final VisitService visitService;

    @Autowired
    public UserMVCController(UserService userService,
                             UserMapper userMapper,
                             UserRoleService userRoleService,
                             VehicleService vehicleService,
                             VisitService visitService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.userRoleService = userRoleService;
        this.vehicleService = vehicleService;
        this.visitService = visitService;
    }

    @ModelAttribute("roles")
    public List<UserRole> populateRoles() {
        return userRoleService.getAll();
    }

    @ModelAttribute("sort")
    public List<UserSortOptions> populateSortOptions() {
        return Arrays.asList(UserSortOptions.values());
    }

    @ModelAttribute("search")
    public SearchDTO populateSearch() {
        return new SearchDTO();
    }

    @ModelAttribute("filter")
    public FilterUserDTO populateFilter() {
        return new FilterUserDTO();
    }

    @ModelAttribute("searchEndPoint")
    public String populateSearchEndPoint() {
        return "/v1/users/search";
    }

    @ModelAttribute("filterEndPoint")
    public String populateFilterEndPoint() {
        return "/v1/users/filter";
    }

    @GetMapping
    public String showUsersPage(Model model) {
        model.addAttribute("users", userService.getAll());
        return "user-all";
    }

    @GetMapping("/{id}")
    public String showUserPage(@PathVariable long id,
                               Model model) {
        try {
            User user = userService.getByID(id);
            model.addAttribute("user", user);
            List<Vehicle> vehicles = vehicleService.getUserVehicles(user);
            model.addAttribute("vehicles", vehicles);
            List<Visit> visits = visitService.getUserVisits(user);
            model.addAttribute("visits", visits);
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/account")
    public String showUserAccountPage(@PathVariable long id,
                                      Model model) {
        try {
            User user = userService.getByID(id);
            model.addAttribute("user", user);
            //todo get vehicle by owner - implement getAll(Optional<User> owner)
//            List<Vehicle> vehicles = vehicleService.getAll();
//            model.addAttribute("vehicles", vehicles);
            return "user-account";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showCreateUserPage(Model model) {
        model.addAttribute("user", new UserDTO());
        return "user-new";
    }

    @PostMapping("/new")
    public String createUser(@Valid @ModelAttribute("user") UserDTO userDTO,
                             BindingResult errors,
                             Model model) {
        if (errors.hasErrors())
            return "user-new";
        try {
            User user = userMapper.fromDTO(userDTO);
            userService.create(user);
            return "redirect:/v1/users/" + user.getId();
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "user-new";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdateUserPage(@PathVariable long id,
                                     Model model) {
        try {
            User user = userService.getByID(id);
            UserDTO userDTO = userMapper.toDTO(user);
            model.addAttribute("user", userDTO);
            model.addAttribute("userID", id);
            return "user-account-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable long id,
                             @Valid @ModelAttribute("user") UserDTO userDTO,
                             BindingResult errors,
                             Model model) {
        if (errors.hasErrors())
            return "user-account-update";
        try {
            User user = userMapper.fromDTO(id, userDTO);
            userService.update(user, user);
            return (userService.isClient(user)) ? "redirect:/v1/private" : "redirect:/v1/users/" + user.getId();
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "user-account-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update/password")
    public String showUpdatePasswordPage(@PathVariable long id,
                                         Model model) {
        model.addAttribute("password", new PasswordDTO());
        return "user-account-update-password";
    }

    @PostMapping("/{id}/update/password")
    public String updatePassword(@PathVariable long id,
                                 @Valid @ModelAttribute("password") PasswordDTO passwordDTO,
                                 BindingResult errors,
                                 Model model) {
        if (errors.hasErrors())
            return "user-account-update-password";
        try {
            User user = userService.getByID(id);
            userMapper.updatePassword(user, passwordDTO);
            userService.update(user, user);
            return (userService.isClient(user)) ? "redirect:/v1/private" : "redirect:/v1/users/" + user.getId();
        } catch (IncorrectEntryException e) {
            model.addAttribute("formError", e.getMessage());
            return "user-account-update-password";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/search")
    public String searchUsers(@Valid @ModelAttribute("search") SearchDTO searchDTO,
                              BindingResult bindingResult,
                              Model model) {
        if (bindingResult.hasErrors())
            return "user-all";
        var searchResult = userService.search(
                Optional.ofNullable(searchDTO.getKeyword().isBlank() ? null : searchDTO.getKeyword())
        );
        model.addAttribute("users", searchResult);
        return "user-all";
    }

    @PostMapping("/filter")
    public String filterUsers(@Valid @ModelAttribute("filter") FilterUserDTO filterUserDTO,
                              BindingResult bindingResult,
                              Model model) {
        if (bindingResult.hasErrors())
            return "user-all";
        var filterResult = userService.filter(
                Optional.ofNullable(filterUserDTO.getFirstName().isBlank() ? null : filterUserDTO.getFirstName()),
                Optional.ofNullable(filterUserDTO.getLastName().isBlank() ? null : filterUserDTO.getLastName()),
                Optional.ofNullable(filterUserDTO.getRoleName().isBlank() ? null : filterUserDTO.getRoleName()),
//                Optional.ofNullable(filterUserDTO.getModel().isBlank() ? null : filterUserDTO.getModel()),
//                Optional.ofNullable(filterUserDTO.getBrand().isBlank() ? null : filterUserDTO.getBrand()),
                Optional.empty(),
                Optional.empty(),
                Optional.ofNullable(filterUserDTO.getSort().isBlank() ? null : filterUserDTO.getSort()),
                Optional.ofNullable(filterUserDTO.getOrder().isBlank() ? null : filterUserDTO.getOrder())
        );
        model.addAttribute("users", filterResult);
        return "user-all";
    }

    @GetMapping("/{id}/vehicles")
    public String showUserVehiclesPage(@PathVariable long id,
                                       Model model) {
        try {
            User user = userService.getByID(id);
            List<Vehicle> vehicles = vehicleService.getUserVehicles(user);
            model.addAttribute("vehicles", vehicles);
            return "vehicle-all";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/visits")
    public String showUserVisitsPage(@PathVariable long id,
                                     Model model) {
        try {
            User user = userService.getByID(id);
            List<Visit> visits = visitService.getUserVisits(user);
            model.addAttribute("visits", visits);
            return "visit-all";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable long id,
                             Model model) {
        try {
            userService.delete(id, userService.getAdmin());
            return "redirect:/v1/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}