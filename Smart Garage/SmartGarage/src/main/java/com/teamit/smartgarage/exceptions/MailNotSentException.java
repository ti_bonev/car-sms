package com.teamit.smartgarage.exceptions;

public class MailNotSentException extends RuntimeException {

    public MailNotSentException(String message) {
        super(message);
    }
}