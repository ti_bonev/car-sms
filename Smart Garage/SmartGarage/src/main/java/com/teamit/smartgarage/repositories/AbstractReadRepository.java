package com.teamit.smartgarage.repositories;

import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.repositories.contracts.BaseReadRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

import static java.lang.String.format;

public abstract class AbstractReadRepository<T> implements BaseReadRepository<T> {

    private final Class<T> clazz;
    private final SessionFactory sessionFactory;

    public AbstractReadRepository(Class<T> clazz, SessionFactory sessionFactory) {
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(format("from %s", clazz.getSimpleName()), clazz).list();
        }
    }

    @Override
    public <V> T getByField(String name, V value) {
        final String query = format("from %s where %s =: value ", clazz.getSimpleName(), name);
        final String notFoundMsg = format("%s with %s %s not found", clazz.getSimpleName(), name, value);
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery(query, clazz)
                    .setParameter("value", value)
                    .uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(notFoundMsg));
        }
    }

    @Override
    public T getById(long id) {
        return getByField("id", id);
    }
}