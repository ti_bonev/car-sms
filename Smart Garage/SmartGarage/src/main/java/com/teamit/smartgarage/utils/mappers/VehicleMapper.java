package com.teamit.smartgarage.utils.mappers;

import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.Vehicle;
import com.teamit.smartgarage.models.VehicleModel;
import com.teamit.smartgarage.models.dtos.VehicleBaseDTO;
import com.teamit.smartgarage.models.dtos.VehicleDTO;
import com.teamit.smartgarage.services.contracts.UserService;
import com.teamit.smartgarage.services.contracts.VehicleModelService;
import com.teamit.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VehicleMapper {

    private final VehicleService vehicleService;
    private final VehicleModelService vehicleModelService;
    private final UserService userService;

    @Autowired
    public VehicleMapper(VehicleService vehicleService, VehicleModelService vehicleModelService, UserService userService) {
        this.vehicleService = vehicleService;
        this.vehicleModelService = vehicleModelService;
        this.userService = userService;
    }

    public Vehicle fromDTO(VehicleDTO vehicleDTO) {
        Vehicle vehicle = new Vehicle();
        dtoToObject(vehicleDTO, vehicle);
        return vehicle;
    }

    public Vehicle fromDTO(VehicleBaseDTO vehicleBaseDTO, long vehicleModelID, User owner) {
        Vehicle vehicle = new Vehicle();
        VehicleModel vehicleModel = vehicleModelService.getById(vehicleModelID); // throws ENF
        vehicle.setModel(vehicleModel);
        vehicle.setOwner(owner);
        dtoToObject(vehicleBaseDTO, vehicle);
        return vehicle;
    }

    public Vehicle fromDTO(long id, VehicleDTO vehicleDTO) {
        Vehicle vehicle = vehicleService.getById(id);
        dtoToObject(vehicleDTO, vehicle);
        return vehicle;
    }

    private void dtoToObject(VehicleDTO vehicleDTO, Vehicle vehicle) {
        VehicleModel vehicleModel = vehicleModelService.getByName(vehicleDTO.getModel());
        User user = userService.getByID(vehicleDTO.getOwnerId());
        vehicle.setLicensePlate(vehicleDTO.getLicensePlate());
        vehicle.setVin(vehicleDTO.getVin());
        vehicle.setProductionYear(vehicleDTO.getProductionYear());
        vehicle.setModel(vehicleModel);
        vehicle.setOwner(user);
    }

    private void dtoToObject(VehicleBaseDTO vehicleBaseDTO, Vehicle vehicle) {
        vehicle.setLicensePlate(vehicleBaseDTO.getLicensePlate());
        vehicle.setVin(vehicleBaseDTO.getVin());
        vehicle.setProductionYear(vehicleBaseDTO.getProductionYear());
    }

    public VehicleBaseDTO toDTO(Vehicle vehicle) {
        VehicleBaseDTO vehicleBaseDTO = new VehicleBaseDTO();
        vehicleBaseDTO.setLicensePlate(vehicle.getLicensePlate());
        vehicleBaseDTO.setVin(vehicle.getVin());
        vehicleBaseDTO.setProductionYear(vehicle.getProductionYear());
        return vehicleBaseDTO;
    }

    public Vehicle fromDTO(long id, VehicleBaseDTO vehicleBaseDTO, long vehicleModelID) {
        Vehicle vehicle = vehicleService.getById(id); // throws ENF
        VehicleModel vehicleModel = vehicleModelService.getById(vehicleModelID); // throws ENF
        vehicle.setModel(vehicleModel);
        dtoToObject(vehicleBaseDTO, vehicle);
        return vehicle;
    }
}