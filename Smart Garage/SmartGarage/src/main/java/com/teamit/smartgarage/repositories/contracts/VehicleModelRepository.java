package com.teamit.smartgarage.repositories.contracts;

import com.teamit.smartgarage.models.VehicleModel;

import java.util.List;
import java.util.Optional;

public interface VehicleModelRepository extends BaseCRUDRepository<VehicleModel> {

    VehicleModel getByName(String name);

    List<VehicleModel> search(Optional<String> search);

    List<VehicleModel> filter(Optional<String> brand, Optional<String> sort, Optional<String> order);
}
