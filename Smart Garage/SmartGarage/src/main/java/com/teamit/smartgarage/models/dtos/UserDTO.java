package com.teamit.smartgarage.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDTO {

    @NotNull
    @Email(message = "Email must be valid: ***@***.***")
    private String email;

    @Pattern(regexp = "^(?=.*\\d)[\\d]{10}$", message = "Phone number must be 10 digits.")
    private String phone;

    @Size(min = 2, max = 20, message = "First name must be at least 2 and max 20 symbols.")
    private String firstName;

    @Size(min = 2, max = 20, message = "Last name must be at least 2 and max 20 symbols.")
    private String lastName;

    public UserDTO() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}