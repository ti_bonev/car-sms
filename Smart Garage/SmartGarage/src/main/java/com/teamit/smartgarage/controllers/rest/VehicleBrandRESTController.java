package com.teamit.smartgarage.controllers.rest;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.VehicleBrand;
import com.teamit.smartgarage.models.dtos.VehicleBrandDTO;
import com.teamit.smartgarage.services.contracts.VehicleBrandService;
import com.teamit.smartgarage.utils.mappers.VehicleBrandMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/garage/brands")
public class VehicleBrandRESTController {

    private final VehicleBrandService vehicleBrandService;
    private final VehicleBrandMapper vehicleBrandMapper;

    public VehicleBrandRESTController(VehicleBrandService vehicleBrandService, VehicleBrandMapper vehicleBrandMapper) {
        this.vehicleBrandService = vehicleBrandService;
        this.vehicleBrandMapper = vehicleBrandMapper;
    }

    @GetMapping
    public List<VehicleBrand> getAll(@RequestParam(required = false) Optional<String> search) {
        if (search.isPresent()) {
            return vehicleBrandService.search(search);
        }
        return vehicleBrandService.getAll();
    }

    @GetMapping("/filter")
    public List<VehicleBrand> filter(@RequestParam(required = false) Optional<String> sort,
                                     @RequestParam(required = false) Optional<String> order) {
        try {
            return vehicleBrandService.filter(sort, order);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public VehicleBrand getById(@PathVariable long id) {
        try {
            return vehicleBrandService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void create(@Valid @RequestBody VehicleBrandDTO vehicleBrandDTO) {
        try {
            VehicleBrand vehicleBrand = vehicleBrandMapper.fromDTO(vehicleBrandDTO);
            vehicleBrandService.create(vehicleBrand);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@PathVariable long id, @Valid @RequestBody VehicleBrandDTO vehicleBrandDTO) {
        try {
            VehicleBrand vehicleBrandToUpdate = vehicleBrandMapper.fromDTO(id, vehicleBrandDTO);
            vehicleBrandService.update(vehicleBrandToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        try {
            vehicleBrandService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }
}
