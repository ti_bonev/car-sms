package com.teamit.smartgarage.models;

import javax.persistence.*;

@Entity
@Table(name = "vehicle_models")
public class VehicleModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "model_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    private VehicleBrand vehicleBrand;

    public VehicleModel() {
    }

    public VehicleModel(int id, String name, VehicleBrand vehicleBrand) {
        this.id = id;
        this.name = name;
        this.vehicleBrand = vehicleBrand;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VehicleBrand getBrand() {
        return vehicleBrand;
    }

    public void setBrand(VehicleBrand vehicleBrand) {
        this.vehicleBrand = vehicleBrand;
    }
}
