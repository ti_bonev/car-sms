package com.teamit.smartgarage.utils.mappers;

import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.Vehicle;
import com.teamit.smartgarage.models.Visit;
import com.teamit.smartgarage.models.dtos.VisitDTO;
import com.teamit.smartgarage.models.dtos.VisitDTOOut;
import com.teamit.smartgarage.services.contracts.VehicleService;
import com.teamit.smartgarage.services.contracts.VisitService;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class VisitMapper {

    private final VisitService visitService;
    private final VehicleService vehicleService;

    public VisitMapper(VisitService visitService,
                       VehicleService vehicleService) {
        this.visitService = visitService;
        this.vehicleService = vehicleService;
    }

    public Visit fromDTO(VisitDTO visitDTO) {
        Visit visit = new Visit();
        dtoToObject(visitDTO, visit);
        return visit;
    }

    public Visit fromDTO(long id, VisitDTO visitDTO) {
        Visit visit = visitService.getById(id);
        dtoToObject(visitDTO, visit);
        return visit;
    }

    private void dtoToObject(VisitDTO visitDTO, Visit visit) {
        Vehicle vehicle = vehicleService.getById(visitDTO.getVehicleId());
        User user = vehicle.getOwner();
        visit.setUser(user);
        visit.setVehicle(vehicle);
    }

    public VisitDTOOut toDTOOut(Visit visit) {
        VisitDTOOut visitDTOOut = getFromVisit(visit);
        visitDTOOut.setServices(visit.getServices());
        visitDTOOut.setCurrency("BGN");
        visitDTOOut.setTotalPrice(visitService.getTotalPrice(visit));
        return visitDTOOut;
    }

    public VisitDTOOut toDTOOut(Visit visit, String currency) {
        VisitDTOOut visitDTOOut = getFromVisit(visit);
        visitDTOOut.setServices(
                new HashSet<>(visitService.getAssistancesInCurrency(visit.getId(), currency)));
        visitDTOOut.setCurrency(currency);
        visitDTOOut.setTotalPrice(visitService.getTotalPriceInCurrency(visit, currency));
        return visitDTOOut;
    }

    private VisitDTOOut getFromVisit(Visit visit) {
        VisitDTOOut visitDTOOut = new VisitDTOOut();
        visitDTOOut.setId(visit.getId());
        visitDTOOut.setUser(visit.getUser());
        visitDTOOut.setVehicle(visit.getVehicle());
        visitDTOOut.setDateIn(visit.getDateIn());
        visitDTOOut.setDateOut(visit.getDateOut());
        visitDTOOut.setComplete(visit.isComplete());
        return visitDTOOut;
    }
}