package com.teamit.smartgarage.services.contracts;

import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.Vehicle;

import java.util.List;
import java.util.Optional;

public interface VehicleService {

    List<Vehicle> getAll();

    Vehicle getById(long id);

    void create(Vehicle vehicle);

    void update(Vehicle vehicle);

    void delete(long id);

    List<Vehicle> search(Optional<String> search);

    // List<Vehicle> filter(Optional<String> brand, Optional<String> model, Optional<String> sort, Optional<String> order);

    List<Vehicle> filter(Optional<String> username, Optional<String> brand, Optional<String> model, Optional<String> sort, Optional<String> order);

    List<Vehicle> getUserVehicles(User user);
}