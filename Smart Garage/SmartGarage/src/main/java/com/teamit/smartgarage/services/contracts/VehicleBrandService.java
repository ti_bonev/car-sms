package com.teamit.smartgarage.services.contracts;

import com.teamit.smartgarage.models.VehicleBrand;

import java.util.List;
import java.util.Optional;

public interface VehicleBrandService {

    List<VehicleBrand> getAll();

    VehicleBrand getById(long id);

    VehicleBrand getByName(String name);

    void create(VehicleBrand vehicleBrand);

    void update(VehicleBrand vehicleBrand);

    void delete(long id);

    List<VehicleBrand> search(Optional<String> search);

    List<VehicleBrand> filter(Optional<String> sort, Optional<String> order);
}
