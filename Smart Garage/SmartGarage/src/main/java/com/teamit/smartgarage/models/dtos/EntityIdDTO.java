package com.teamit.smartgarage.models.dtos;

import javax.validation.constraints.Positive;

public class EntityIdDTO {

    @Positive(message = "ID must be positive integer number.")
    private long id;

    public EntityIdDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}