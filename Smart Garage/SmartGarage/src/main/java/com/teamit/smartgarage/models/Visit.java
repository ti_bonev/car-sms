package com.teamit.smartgarage.models;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "visits")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "visits_services", joinColumns = @JoinColumn(name = "visit_id"), inverseJoinColumns = @JoinColumn(name = "service_id"))
    private Set<Assistance> assistances;

    @Column(name = "date_in")
    @Temporal(TemporalType.DATE)
    @CreationTimestamp
    private Date dateIn;

    @Column(name = "date_out")
    @Temporal(TemporalType.DATE)
    @UpdateTimestamp
    private Date dateOut;

    @Column(name = "is_complete")
    private boolean isComplete;

    public Visit() {
    }

    public Visit(int id, User user, Vehicle vehicle, Set<Assistance> assistances, Date dateIn, Date dateOut) {
        this.id = id;
        this.user = user;
        this.vehicle = vehicle;
        this.assistances = assistances;
        this.dateIn = dateIn;
        this.dateOut = dateOut;
        this.isComplete = false;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Set<Assistance> getServices() {
        return assistances;
    }

    public void setServices(Set<Assistance> assistances) {
        this.assistances = assistances;
    }

    public Date getDateIn() {
        return dateIn;
    }

    public void setDateIn(Date dateIn) {
        this.dateIn = dateIn;
    }

    public Date getDateOut() {
        return dateOut;
    }

    public void setDateOut(Date dateOut) {
        this.dateOut = dateOut;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
