package com.teamit.smartgarage.utils.currencyconverter;

import com.teamit.smartgarage.exceptions.CurrencyConversionException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;

public class CurrencyConverter {

    /**
     * Gives the exchange rate for 1 BGN to selected foreign currency.
     * Uses an external API to get conversion rates in real time.
     * API website: https://www.currencyconverterapi.com/
     *
     * @param currencyTo = the code of the currency to get exchange rate for, according to the ISO 4217 standard.
     * @return exchange rate for 1 BGN to selected foreign currency (return type double).
     * @throws CurrencyConversionException;
     */
    public static double getExchangeRates(String currencyTo) {
        String getUrl = "https://free.currconv.com/api/v7/convert?q=BGN_" + currencyTo + "&compact=ultra&apiKey=4a0c374229d7785519c7";
        double exchangeRate;

        try {
            URL url = new URL(getUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            int responseCode = httpURLConnection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                JSONObject obj = new JSONObject(response.toString());
                String jsonKey = "BGN_" + currencyTo;
                exchangeRate = obj.getDouble(jsonKey);
            } else throw new CurrencyConversionException("Currency conversion service is unavailable at this time.");
        } catch (IOException | JSONException e) {
            throw new CurrencyConversionException("Currency conversion service is unavailable at this time.");
        }
        return exchangeRate;
    }

    /**
     * Calculates the price of a service in the selected foreign currency.
     *
     * @param price        = price in BGN as stored in database.
     * @param exchangeRate = exchange rate for 1 BGN to selected foreign currency.
     * @return the price converted to selected foreign currency (return type double).
     */
    public static double convertPrice(double price, double exchangeRate) {
        double convertedPrice = price * exchangeRate;
        convertedPrice = formatPrice(convertedPrice);
        return convertedPrice;
    }

    /**
     * Accepts double. Formats the price to 2 symbols after the decimal point.
     *
     * @param price = the price to be formatted.
     * @return formatted price to 2 symbols after the decimal point (return type double).
     */
    private static double formatPrice(double price) {
        BigDecimal priceToFormat = new BigDecimal(price);
        priceToFormat = priceToFormat.setScale(2, RoundingMode.HALF_UP);
        double formattedPrice = priceToFormat.doubleValue();
        return formattedPrice;
    }
}
