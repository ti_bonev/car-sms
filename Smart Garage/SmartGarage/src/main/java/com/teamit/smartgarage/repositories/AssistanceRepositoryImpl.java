package com.teamit.smartgarage.repositories;

import com.teamit.smartgarage.models.Assistance;
import com.teamit.smartgarage.repositories.contracts.AssistanceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class AssistanceRepositoryImpl extends AbstractCRUDRepository<Assistance> implements AssistanceRepository {

    @Autowired
    public AssistanceRepositoryImpl(SessionFactory sessionFactory) {
        super(Assistance.class, sessionFactory);
    }

    @Override
    public Assistance getByName(String name) {
        return getByField("name", name);
    }

    @Override
    public List<Assistance> search(Optional<String> search) {
        try (Session session = sessionFactory.openSession()) {
            if (search.isEmpty()) {
                return getAll();
            }
            Query<Assistance> query = session.createQuery(" from Assistance where " +
                    " name like :search ", Assistance.class);
            query.setParameter("search", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<Assistance> filter(Optional<String> name, Optional<Double> priceFrom, Optional<Double> priceTo, Optional<String> sort, Optional<String> order) {
        try (Session session = sessionFactory.openSession()) {
            var queryBuild = new StringBuilder(" from Assistance ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();
            name.ifPresent(value -> {
                filter.add(" name like :name ");
                queryParams.put("name", "%" + value + "%");
            });
            priceFrom.ifPresent(value -> {
                filter.add(" price >= :priceFrom ");
                queryParams.put("priceFrom", value);
            });
            priceTo.ifPresent(value -> {
                filter.add(" price <= :priceTo ");
                queryParams.put("priceTo", value);
            });
            if (!filter.isEmpty()) {
                queryBuild.append(" where ").append(String.join(" and ", filter));
            }
            sort.ifPresent(value -> queryBuild.append(sortBy(value, order)));
            Query<Assistance> query = session.createQuery(queryBuild.toString(), Assistance.class);
            query.setProperties(queryParams);
            System.out.println(queryBuild);
            return query.list();
        }
    }

    private String sortBy(String value, Optional<String> order) {
        var queryBuild = new StringBuilder(" order by ");
        switch (value) {
            case "name":
                queryBuild.append(" name ");
                break;
            case "price":
                queryBuild.append(" price ");
                break;
            default:
                throw new UnsupportedOperationException("Type mismatch. Choose 'name' or 'price'.");
        }
        order.ifPresent(sortOrder -> {
            switch (sortOrder) {
                case "ascending":
                    queryBuild.append(" asc ");
                    break;
                case "descending":
                    queryBuild.append(" desc ");
                    break;
                default:
                    throw new UnsupportedOperationException(
                            "Type mismatch. Choose 'ascending' or 'descending'."
                    );
            }
        });
        return queryBuild.toString();
    }
}
