package com.teamit.smartgarage.models.dtos;

public class VisitDTO {

    private long vehicleId;

    public VisitDTO() {
    }

    public long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }
}