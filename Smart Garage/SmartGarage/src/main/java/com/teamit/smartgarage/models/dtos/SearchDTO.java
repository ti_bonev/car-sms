package com.teamit.smartgarage.models.dtos;

import javax.validation.constraints.Size;

public class SearchDTO {

    @Size(min = 2, message = "Type at least 2 symbols to perform search.")
    private String keyword;

    public SearchDTO() {
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}