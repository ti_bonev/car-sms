package com.teamit.smartgarage.models.dtos;

import javax.validation.constraints.Size;

public class VehicleModelDTO {

    @Size(min = 2, max = 32, message = "Model name should be between 2 and 32 symbols.")
    private String name;

    private String brandName;

    public VehicleModelDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
