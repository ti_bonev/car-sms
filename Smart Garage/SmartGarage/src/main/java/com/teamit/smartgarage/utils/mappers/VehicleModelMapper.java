package com.teamit.smartgarage.utils.mappers;

import com.teamit.smartgarage.models.VehicleBrand;
import com.teamit.smartgarage.models.VehicleModel;
import com.teamit.smartgarage.models.dtos.VehicleModelDTO;
import com.teamit.smartgarage.services.contracts.VehicleBrandService;
import com.teamit.smartgarage.services.contracts.VehicleModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VehicleModelMapper {

    private final VehicleModelService vehicleModelService;
    private final VehicleBrandService vehicleBrandService;

    @Autowired
    public VehicleModelMapper(VehicleModelService vehicleModelService, VehicleBrandService vehicleBrandService) {
        this.vehicleModelService = vehicleModelService;
        this.vehicleBrandService = vehicleBrandService;
    }

    public VehicleModel fromDTO(VehicleModelDTO vehicleModelDTO) {
        VehicleModel vehicleModel = new VehicleModel();
        dtoToObject(vehicleModelDTO, vehicleModel);
        return vehicleModel;
    }

    public VehicleModel fromDTO(long id, VehicleModelDTO vehicleModelDTO) {
        VehicleModel vehicleModel = vehicleModelService.getById(id);
        dtoToObject(vehicleModelDTO, vehicleModel);
        return vehicleModel;
    }

    private void dtoToObject(VehicleModelDTO vehicleModelDTO, VehicleModel vehicleModel) {
        VehicleBrand vehicleBrand = vehicleBrandService.getByName(vehicleModelDTO.getBrandName());
        vehicleModel.setName(vehicleModelDTO.getName());
        vehicleModel.setBrand(vehicleBrand);
    }
}
