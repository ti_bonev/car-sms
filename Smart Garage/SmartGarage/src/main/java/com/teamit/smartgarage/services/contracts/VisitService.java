package com.teamit.smartgarage.services.contracts;

import com.teamit.smartgarage.models.Assistance;
import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.Visit;

import java.util.List;
import java.util.Optional;

public interface VisitService {

    List<Visit> getAll();

    Visit getById(long id);

    void create(Visit visit);

    void update(Visit visit);

    void delete(long id);

    void addServiceToVisit(long id, Assistance assistance);

    void removeServiceFromVisit(long id, Assistance assistance);

    void closeVisit(long id);

    List<Visit> search(Optional<String> search);

    List<Visit> filter(Optional<String> username,
                       Optional<String> brand,
                       Optional<String> model,
                       Optional<String> licensePlate,
                       Optional<String> vin,
                       Optional<String> dateFrom,
                       Optional<String> dateTo,
                       Optional<String> sort,
                       Optional<String> order);

    List<Visit> getUserVisits(User user);

    List<Assistance> getAssistancesInCurrency(long id, String currency);

    double getTotalPrice(Visit visit);

    double getTotalPriceInCurrency(Visit visit, String Currency);

    void sendVisitReportEmail(Visit visit);
//todo send report appears only after closing visit and cant take visitDtoOut
//    void sendVisitReportEmailInCurrency(Visit visit, String currency);
}
