package com.teamit.smartgarage.utils.mappers;

import com.teamit.smartgarage.models.Assistance;
import com.teamit.smartgarage.models.dtos.AssistanceDTO;
import com.teamit.smartgarage.services.contracts.AssistanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AssistanceMapper {

    private final AssistanceService assistanceService;

    @Autowired
    public AssistanceMapper(AssistanceService assistanceService) {
        this.assistanceService = assistanceService;
    }

    public Assistance fromDTO(AssistanceDTO assistanceDTO) {
        Assistance assistance = new Assistance();
        dtoToObject(assistanceDTO, assistance);
        return assistance;
    }

    public Assistance fromDTO(long id, AssistanceDTO assistanceDTO) {
        Assistance assistance = assistanceService.getById(id);
        dtoToObject(assistanceDTO, assistance);
        return assistance;
    }

    private void dtoToObject(AssistanceDTO assistanceDTO, Assistance assistance) {
        assistance.setName(assistanceDTO.getName());
        assistance.setPrice(assistanceDTO.getPrice());
    }

    public AssistanceDTO toDTO(Assistance assistance) {
        AssistanceDTO assistanceDTO = new AssistanceDTO();
        assistanceDTO.setName(assistance.getName());
        assistanceDTO.setPrice(assistance.getPrice());
        return assistanceDTO;
    }
}