package com.teamit.smartgarage.exceptions;

public class CurrencyConversionException extends RuntimeException {

    public CurrencyConversionException(String message) {
        super(message);
    }
}
