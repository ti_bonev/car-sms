package com.teamit.smartgarage.models.dtos;

import javax.validation.constraints.Size;

public class VehicleBrandDTO {

    @Size(min = 2, max = 32, message = "Brand name should be between 2 and 32 symbols.")
    private String name;

    public VehicleBrandDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
