package com.teamit.smartgarage.services;

import com.teamit.smartgarage.exceptions.MailNotSentException;
import com.teamit.smartgarage.models.Mail;
import com.teamit.smartgarage.services.contracts.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Service
public class MailServiceImpl implements MailService {
/*
- Mark MailServiceImpl class as "mailService" using @Service annotation.
- Use @Autowired annotation to autowire JavaMailSender class.
- javax.mail.internet.MimeMessage class represents a MIME style email message. Spring MimeMessageHelper class
offers support for HTML text content, inline elements such as images, and typical mail attachments.
- Spring MimeMessageHelper.getMimeMessage() return the populated underlying MimeMessage object along with attachments.
- Finally use JavaMailSender.send() method to send populated MIME message.

- MIME: A media type (also known as a Multipurpose Internet Mail Extensions or MIME type)
indicates the nature and format of a document, file, or assortment of bytes.
A MIME type most-commonly consists of just two parts: a type and a subtype, separated by a slash (/)
 with no whitespace between: type/subtype.
 There are two classes of type: discrete and multipart.
 */

    @Autowired
    JavaMailSender mailSender;

    @Override
    public void sendEmail(String email, String subject, String content) {
        Mail mail = new Mail();
        mail.setMailTo(email);
        mail.setSubject(subject);
        mail.setContent(content);
        sendEmail(mail);
    }

    private void sendEmail(Mail mail) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            mimeMessageHelper.setSubject(mail.getSubject());
            // in 'From' section sets "Mighty IT Team" instead of "mighty.it.team.07@gmail.com"
            mimeMessageHelper.setFrom(new InternetAddress(mail.getMailFrom(), "Mighty IT Team"));
            mimeMessageHelper.setTo(mail.getMailTo());
            mimeMessageHelper.setText(mail.getContent());
            mailSender.send(mimeMessageHelper.getMimeMessage());
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new MailNotSentException(e.getMessage());
        }
    }
}