package com.teamit.smartgarage.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class EmailDTO {

    @NotNull
    @Email(message = "Email must be valid: ***@***.***")
    private String email;

    public EmailDTO() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}