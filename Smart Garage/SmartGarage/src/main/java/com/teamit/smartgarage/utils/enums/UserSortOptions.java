package com.teamit.smartgarage.utils.enums;

public enum UserSortOptions {
    firstName,
    lastName,
    role
}