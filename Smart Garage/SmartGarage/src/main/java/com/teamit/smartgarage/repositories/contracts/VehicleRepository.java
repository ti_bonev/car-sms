package com.teamit.smartgarage.repositories.contracts;

import com.teamit.smartgarage.models.Vehicle;

import java.util.List;
import java.util.Optional;

public interface VehicleRepository extends BaseCRUDRepository<Vehicle> {

    List<Vehicle> search(Optional<String> search);

    List<Vehicle> filter(Optional<String> username, Optional<String> brand, Optional<String> model, Optional<String> sort, Optional<String> order);
}
