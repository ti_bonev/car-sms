package com.teamit.smartgarage.services;

import com.teamit.smartgarage.models.UserRole;
import com.teamit.smartgarage.repositories.contracts.UserRoleRepository;
import com.teamit.smartgarage.services.contracts.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService {
    private final UserRoleRepository userRoleRepository;

    @Autowired
    public UserRoleServiceImpl(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public List<UserRole> getAll() {
        return userRoleRepository.getAll();
    }

    @Override
    public UserRole getByID(long id) {
        return userRoleRepository.getById(id);
    }

    @Override
    public UserRole getByName(String role) {
        return userRoleRepository.getByField("role", role);
    }
}
