package com.teamit.smartgarage.models.dtos;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class AssistanceDTO {

    @Size(min = 2, max = 32, message = "Service title should be between 2 and 32 symbols.")
    private String name;

    @Positive
    private double price;

    public AssistanceDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
