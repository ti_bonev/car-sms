package com.teamit.smartgarage.repositories;

import com.teamit.smartgarage.models.VehicleModel;
import com.teamit.smartgarage.repositories.contracts.VehicleModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class VehicleModelRepositoryImpl extends AbstractCRUDRepository<VehicleModel> implements VehicleModelRepository {

    @Autowired
    public VehicleModelRepositoryImpl(SessionFactory sessionFactory) {
        super(VehicleModel.class, sessionFactory);
    }

    @Override
    public VehicleModel getByName(String name) {
        return getByField("name", name);
    }

    @Override
    public List<VehicleModel> search(Optional<String> search) {
        try (Session session = sessionFactory.openSession()) {
            if (search.isEmpty()) {
                return getAll();
            }
            Query<VehicleModel> query = session.createQuery(" from VehicleModel where " +
                    " name like :search or " +
                    " vehicleBrand.name like :search ", VehicleModel.class);
            query.setParameter("search", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<VehicleModel> filter(Optional<String> brand, Optional<String> sort, Optional<String> order) {
        try (Session session = sessionFactory.openSession()) {
            var queryBuild = new StringBuilder(" from VehicleModel ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();
            brand.ifPresent(value -> {
                filter.add(" vehicleBrand.name like :brand ");
                queryParams.put("brand", "%" + value + "%");
            });
            if (!filter.isEmpty()) {
                queryBuild.append(" where ").append(String.join(" and ", filter));
            }
            sort.ifPresent(value -> queryBuild.append(sortBy(value, order)));
            Query<VehicleModel> query = session.createQuery(queryBuild.toString(), VehicleModel.class);
            query.setProperties(queryParams);
            return query.list();
        }
    }

    private String sortBy(String value, Optional<String> order) {
        var queryBuild = new StringBuilder(" order by ");
        switch (value) {
            case "model":
                queryBuild.append(" name ");
                break;
            case "brand":
                queryBuild.append(" vehicleBrand.name ");
                break;
            default:
                throw new UnsupportedOperationException("Type mismatch. Choose 'brand'.");
        }
        order.ifPresent(sortOrder -> {
            switch (sortOrder) {
                case "ascending":
                    queryBuild.append(" asc ");
                    break;
                case "descending":
                    queryBuild.append(" desc ");
                    break;
                default:
                    throw new UnsupportedOperationException(
                            "Type mismatch. Choose 'ascending' or 'descending'."
                    );
            }
        });
        return queryBuild.toString();
    }
}
