package com.teamit.smartgarage.controllers.mvc;

import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.dtos.SearchDTO;
import com.teamit.smartgarage.services.contracts.UserService;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;

@ControllerAdvice
public class AnnotationAdvice {
    /* @ControllerAdvice is a specialization of @Component which can be used to define methods with
    @ExceptionHandler, @InitBinder, and @ModelAttribute annotations. Such methods are applied to all
    @RequestMapping methods across multiple @Controller classes instead of just local @Controller class.
     */

    private final UserService userService;

    public AnnotationAdvice(UserService userService) {
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean isAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUser")
    public User getCurrentUser(HttpSession session) {
        return isAuthenticated(session) ? getUser(session) : null;
    }

    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        return isAuthenticated(session) && isInRole(getUser(session), "admin");
    }

    @ModelAttribute("isEmployee")
    public boolean isEmployee(HttpSession session) {
        return isAuthenticated(session) && isInRole(getUser(session), "employee");
    }

    @ModelAttribute("isAdminOrEmployee")
    public boolean isAdminOrEmployee(HttpSession session) {
        return isAuthenticated(session)
                && (isInRole(getUser(session), "admin")
                || isInRole(getUser(session), "employee"));
    }

    @ModelAttribute("isClient")
    public boolean isClient(HttpSession session) {
        return isAuthenticated(session) && isInRole(getUser(session), "client");
    }

    @ModelAttribute("search")
    public SearchDTO populateSearch() {
        return new SearchDTO();
    }

    private User getUser(HttpSession session) {
        return userService.getByUsername(session.getAttribute("currentUser").toString());
    }

    private boolean isInRole(User user, String roleName) {
        return user.getRole().getRoleName().equals(roleName);
    }
}