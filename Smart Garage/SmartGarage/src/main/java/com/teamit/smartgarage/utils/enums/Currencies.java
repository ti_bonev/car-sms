package com.teamit.smartgarage.utils.enums;

public enum Currencies {
    BGN,
    EUR,
    USD,
    RUB,
    TRY,
    RON,
    RSD
}