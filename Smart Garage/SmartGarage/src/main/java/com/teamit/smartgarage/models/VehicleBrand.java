package com.teamit.smartgarage.models;

import javax.persistence.*;

@Entity
@Table(name = "vehicle_brands")
public class VehicleBrand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "brand_name")
    private String name;

    public VehicleBrand() {
    }

    public VehicleBrand(int id, String brandName) {
        this.id = id;
        this.name = brandName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
