package com.teamit.smartgarage.repositories.contracts;

import com.teamit.smartgarage.models.Visit;

import java.util.List;
import java.util.Optional;

public interface VisitRepository extends BaseCRUDRepository<Visit> {

    List<Visit> search(Optional<String> search);

    List<Visit> filter(Optional<String> username,
                       Optional<String> brand,
                       Optional<String> model,
                       Optional<String> licensePlate,
                       Optional<String> vin,
                       Optional<String> dateFrom,
                       Optional<String> dateTo,
                       Optional<String> sort,
                       Optional<String> order);
}
