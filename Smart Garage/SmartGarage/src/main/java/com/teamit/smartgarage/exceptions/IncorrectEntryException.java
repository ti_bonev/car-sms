package com.teamit.smartgarage.exceptions;

public class IncorrectEntryException extends RuntimeException {

    public IncorrectEntryException(String message) {
        super(message);
    }
}