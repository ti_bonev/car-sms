package com.teamit.smartgarage.controllers.mvc;

import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.dtos.LogInDTO;
import com.teamit.smartgarage.models.dtos.SearchDTO;
import com.teamit.smartgarage.models.dtos.UserDTOOut;
import com.teamit.smartgarage.services.contracts.AssistanceService;
import com.teamit.smartgarage.services.contracts.UserService;
import com.teamit.smartgarage.services.contracts.VehicleService;
import com.teamit.smartgarage.services.contracts.VisitService;
import com.teamit.smartgarage.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/v1")
public class HomeMVCController {
    private final UserService userService;
    private final VisitService visitService;
    private final VehicleService vehicleService;
    private final AssistanceService assistanceService;
    private final UserMapper userMapper;

    @Autowired
    public HomeMVCController(UserService userService,
                             VisitService visitService,
                             VehicleService vehicleService,
                             AssistanceService assistanceService,
                             UserMapper userMapper) {
        this.userService = userService;
        this.visitService = visitService;
        this.vehicleService = vehicleService;
        this.assistanceService = assistanceService;
        this.userMapper = userMapper;
    }

    @GetMapping
    public String exploreSmartGarage() {
        return "index";
    }

    @GetMapping("/home")
    public String showHomePage(Model model) {
        model.addAttribute("login", new LogInDTO());
        return "home";
    }

    @GetMapping("/private")
    public String showPrivatePartPage(HttpSession session,
                                      Model model) {
        User user = userService.getByUsername((String) session.getAttribute("currentUser"));
        UserDTOOut userDTOOut = userMapper.toDTOOut(user);
        model.addAttribute("user", userDTOOut);
        model.addAttribute("visits", userDTOOut.getVisits());
        model.addAttribute("services", assistanceService.getAll());
// todo visitService.getReportAsString(); - can't show in modal
        model.addAttribute("search", new SearchDTO());
        return "client-home";
    }

    @PostMapping("/private/search")
    public String searchUsers(@Valid @ModelAttribute("search") SearchDTO searchDTO,
                              BindingResult bindingResult,
                              Model model) {
        if (bindingResult.hasErrors())
            return "client-home";
        //todo not working correctly: should search in User's list of visits, not in whole db. Implement new method.
        var searchResult = visitService.search(
                Optional.ofNullable(searchDTO.getKeyword().isBlank() ? null : searchDTO.getKeyword())
        );
        model.addAttribute("visits", searchResult);
        return "client-home";
    }

    @GetMapping("/private/visits/{id}/send")
    public String sendReport(@PathVariable long id) {
        visitService.sendVisitReportEmail(visitService.getById(id));
        return "redirect:/v1/private";
    }

    @GetMapping("/contacts")
    public String showContacts() {
        return "contacts";
    }

}