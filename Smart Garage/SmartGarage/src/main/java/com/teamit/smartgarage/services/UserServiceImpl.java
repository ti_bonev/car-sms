package com.teamit.smartgarage.services;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.exceptions.UnauthorizedOperationException;
import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.Vehicle;
import com.teamit.smartgarage.models.Visit;
import com.teamit.smartgarage.repositories.contracts.UserRepository;
import com.teamit.smartgarage.services.contracts.MailService;
import com.teamit.smartgarage.services.contracts.UserService;
import com.teamit.smartgarage.services.contracts.VehicleService;
import com.teamit.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    public static final String EMAIL_HEADER = "Welcome to Mighty IT Smart garage Car Service!\n" +
            "You can see your visit details and provided services after log in our web site:\n" +
            "\n" +
            "www.mit-smart-garage.com (coming soon)\n" +
            "\n";
    public static final String EMAIL_FOOTER = "\n" +
            "Thank you for choosing our services!\n" +
            "\n" +
            "Mighty IT Team\n" +
            "Ivo & Tosho\n" +
            "\n" +
            "Ivaylo Ivanov : 0885 63 20 31\n" +
            "Todor Bonev   : 0878 54 55 02";

    private final UserRepository userRepository;
    private final MailService mailService;
    private final VehicleService vehicleService;
    private final VisitService visitService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           MailService mailService,
                           VehicleService vehicleService,
                           VisitService visitService) {
        this.userRepository = userRepository;
        this.mailService = mailService;
        this.vehicleService = vehicleService;
        this.visitService = visitService;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getByID(long id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByField("username", username);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByField("email", email);
    }

    @Override
    public User getByPhone(String phone) {
        return userRepository.getByField("phone", phone);
    }

    @Override
    public void create(User user) {
        boolean hasDuplicate = true;
        try {
            getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            hasDuplicate = false;
        }
        if (hasDuplicate)
            throw new DuplicateEntityException("User", "e-mail", user.getEmail());
        hasDuplicate = true;
        try {
            getByPhone(user.getPhone());
        } catch (EntityNotFoundException e) {
            hasDuplicate = false;
        }
        if (hasDuplicate)
            throw new DuplicateEntityException("User", "phone", user.getPhone());
        //create & updateUsername will be best to pass both as transaction
        userRepository.create(user);
        updateUsername(user);
        sendRegistrationEmail(user);
    }

    @Override
    public void update(User userToUpdate, User currentUser) {
        if (isClient(currentUser) && currentUser.getId() != userToUpdate.getId()) {
            throw new UnauthorizedOperationException("Only employee, admin or account owner can update user details.");
        }
        boolean hasDuplicate = true;
        try {
            User existingUser = getByEmail(userToUpdate.getEmail());
            if (existingUser.getId() == userToUpdate.getId())
                hasDuplicate = false;
        } catch (EntityNotFoundException e) {
            hasDuplicate = false;
        }
        if (hasDuplicate)
            throw new DuplicateEntityException("User", "e-mail", userToUpdate.getEmail());
        hasDuplicate = true;
        try {
            User existingUser = getByUsername(userToUpdate.getUsername());
            if (existingUser.getId() == userToUpdate.getId())
                hasDuplicate = false;
        } catch (EntityNotFoundException e) {
            hasDuplicate = false;
        }
        if (hasDuplicate)
            throw new DuplicateEntityException("User", "username", userToUpdate.getUsername());
        hasDuplicate = true;
        try {
            User existingUser = getByPhone(userToUpdate.getPhone());
            if (existingUser.getId() == userToUpdate.getId())
                hasDuplicate = false;
        } catch (EntityNotFoundException e) {
            hasDuplicate = false;
        }
        if (hasDuplicate)
            throw new DuplicateEntityException("User", "phone", userToUpdate.getPhone());
        userRepository.update(userToUpdate);
    }

    @Override
    public void delete(long id, User currentUser) {
        if (isClient(currentUser) && currentUser.getId() != getByID(id).getId()) {
            throw new UnauthorizedOperationException("Only employee, admin or account owner can delete user account.");
        }
        deleteVisitsOfUser(id);
        deleteVehiclesOfUser(id);
        userRepository.delete(id);
    }

    @Override
    public boolean isClient(User user) {
        return user.getRole().getRoleName().equals("client");
    }

    @Override
    public boolean isEmployee(User user) {
        return user.getRole().getRoleName().equals("employee");
    }

    @Override
    public boolean isAdmin(User user) {
        return user.getRole().getRoleName().equals("admin");
    }

    private void updateUsername(User user) {
        String username = generateUsername(user);
        user.setUsername(username);
        userRepository.update(user);
    }

    private String generateUsername(User user) {
        String emailPart = user.getEmail().substring(0, user.getEmail().indexOf('@'));
        String idPart = Long.toString(user.getId());
        return emailPart + "_" + idPart;
    }

    private void sendRegistrationEmail(User user) {
        String email = user.getEmail();
        String subject = "Registration details for Mighty IT Smart garage Car Service";
        String content = String.format(EMAIL_HEADER +
                        "with provided credentials:\n" +
                        "\n" +
                        "username: %s\n" +
                        "password: %s\n" +
                        EMAIL_FOOTER,
                user.getUsername(),
                user.getPassword());
        mailService.sendEmail(email, subject, content);
    }

    @Override
    public void sendNewPasswordEmail(User user) {
        String email = user.getEmail();
        String subject = "New password for Mighty IT Smart garage Car Service";
        String content = String.format(EMAIL_HEADER +
                        "You received this e-mail because you chose the forgotten password option.\n" +
                        "Your new password for the account is:%n%n%s%n%n" +
                        EMAIL_FOOTER,
                user.getPassword());
        mailService.sendEmail(email, subject, content);
    }

    @Override
    public List<User> search(Optional<String> search) {
        return userRepository.search(search);
    }

    @Override
    public List<User> filter(Optional<String> firstName,
                             Optional<String> lastName,
                             Optional<String> role,
                             Optional<String> model,
                             Optional<String> brand,
                             Optional<String> sort,
                             Optional<String> order) {
        return userRepository.filter(firstName, lastName, role, model, brand, sort, order);
    }

    @Override
    public User getAdmin() {
        return getAllAdmins()
                .stream()
                .findFirst()
                .get();
    }

    private List<User> getAllAdmins() {
        return userRepository.filter(Optional.empty(),
                Optional.empty(),
                Optional.of("admin"),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty()
        );
    }

    private void deleteVehiclesOfUser(long id) {
        List<Vehicle> vehiclesOfUser = vehicleService.getUserVehicles(getByID(id));
        vehiclesOfUser.forEach(vehicle -> vehicleService.delete(vehicle.getId()));
    }

    private void deleteVisitsOfUser(long id) {
        List<Visit> visitsOfUser = visitService.getUserVisits(getByID(id));
        visitsOfUser.forEach(visit -> visitService.delete(visit.getId()));
    }
}