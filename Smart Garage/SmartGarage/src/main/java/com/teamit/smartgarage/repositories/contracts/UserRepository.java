package com.teamit.smartgarage.repositories.contracts;

import com.teamit.smartgarage.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseCRUDRepository<User> {

    List<User> search(Optional<String> search);

    List<User> filter(Optional<String> firstName,
                      Optional<String> lastName,
                      Optional<String> role,
                      Optional<String> model,
                      Optional<String> brand,
                      Optional<String> sort,
                      Optional<String> order);
}