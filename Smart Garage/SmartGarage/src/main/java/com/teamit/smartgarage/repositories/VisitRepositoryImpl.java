package com.teamit.smartgarage.repositories;

import com.teamit.smartgarage.models.Visit;
import com.teamit.smartgarage.repositories.contracts.VisitRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class VisitRepositoryImpl extends AbstractCRUDRepository<Visit> implements VisitRepository {

    @Autowired
    public VisitRepositoryImpl(SessionFactory sessionFactory) {
        super(Visit.class, sessionFactory);
    }

    @Override
    public List<Visit> search(Optional<String> search) {
        try (Session session = sessionFactory.openSession()) {
            if (search.isEmpty()) {
                return getAll();
            }
            Query<Visit> query = session.createQuery(" from Visit where " +
                    " vehicle.licensePlate like :search or " +
                    " vehicle.vin like :search or " +
                    " user.username like :search or" +
                    " user.firstName like :search or " +
                    " user.lastName like :search or " +
                    " user.phone like :search or " +
                    " vehicle.vehicleModel.name like :search or " +
                    " vehicle.vehicleModel.vehicleBrand.name like :search ", Visit.class);
            query.setParameter("search", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<Visit> filter(Optional<String> username,
                              Optional<String> brand,
                              Optional<String> model,
                              Optional<String> licensePlate,
                              Optional<String> vin,
                              Optional<String> dateFrom,
                              Optional<String> dateTo,
                              Optional<String> sort,
                              Optional<String> order) {

        try (Session session = sessionFactory.openSession()) {
            var queryBuild = new StringBuilder(" from Visit ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();
            username.ifPresent(value -> {
                filter.add(" user.username like :username ");
                queryParams.put("username", "%" + value + "%");
            });
            brand.ifPresent(value -> {
                filter.add(" vehicle.vehicleModel.vehicleBrand.name like :brand ");
                queryParams.put("brand", "%" + value + "%");
            });
            model.ifPresent(value -> {
                filter.add(" vehicle.vehicleModel.name like :model ");
                queryParams.put("model", "%" + value + "%");
            });
            licensePlate.ifPresent(value -> {
                filter.add(" vehicle.licensePlate like :licensePlate ");
                queryParams.put("licensePlate", "%" + value + "%");
            });
            vin.ifPresent(value -> {
                filter.add(" vehicle.vin like :vin ");
                queryParams.put("vin", "%" + value + "%");
            });
            dateFrom.ifPresent(value -> {
                filter.add(" dateIn >= DATE_FORMAT(:dateFrom,'%Y,%m,%d') ");
                queryParams.put("dateFrom", value);
            });
            dateTo.ifPresent(value -> {
                filter.add(" dateIn <= DATE_FORMAT(:dateTo,'%Y,%m,%d') ");
                queryParams.put("dateTo", value);
            });
            if (!filter.isEmpty()) {
                queryBuild.append(" where ").append(String.join(" and ", filter));
            }
            sort.ifPresent(value -> queryBuild.append(sortBy(value, order)));
            Query<Visit> query = session.createQuery(queryBuild.toString(), Visit.class);
            query.setProperties(queryParams);
            return query.list();
        }
    }

    private String sortBy(String value, Optional<String> order) {
        var queryBuild = new StringBuilder(" order by ");
        switch (value) {
            case "model":
                queryBuild.append(" vehicle.vehicleModel.name ");
                break;
            case "brand":
                queryBuild.append(" vehicle.vehicleModel.vehicleBrand.name ");
                break;
            case "date":
                queryBuild.append(" dateIn ");
            default:
                throw new UnsupportedOperationException("Type mismatch. Choose 'date', 'brand' or 'model'.");
        }
        order.ifPresent(sortOrder -> {
            switch (sortOrder) {
                case "ascending":
                    queryBuild.append(" asc ");
                    break;
                case "descending":
                    queryBuild.append(" desc ");
                    break;
                default:
                    throw new UnsupportedOperationException(
                            "Type mismatch. Choose 'ascending' or 'descending'."
                    );
            }
        });
        return queryBuild.toString();
    }
}
