package com.teamit.smartgarage.models;

import javax.persistence.*;

@Entity
@Table(name = "vehicles")
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "license_plate")
    private String licensePlate;

    @Column(name = "vin")
    private String vin;

    @Column(name = "production_year")
    private int productionYear;

    @ManyToOne
    @JoinColumn(name = "model_id")
    private VehicleModel vehicleModel;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    public Vehicle() {
    }

    public Vehicle(int id, String licensePlate, String vin, int productionYear, VehicleModel vehicleModel, User owner) {
        this.id = id;
        this.licensePlate = licensePlate;
        this.vin = vin;
        this.productionYear = productionYear;
        this.vehicleModel = vehicleModel;
        this.owner = owner;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(int productionYear) {
        this.productionYear = productionYear;
    }

    public VehicleModel getModel() {
        return vehicleModel;
    }

    public void setModel(VehicleModel vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
