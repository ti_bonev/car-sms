package com.teamit.smartgarage.controllers.rest;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.VehicleModel;
import com.teamit.smartgarage.models.dtos.VehicleModelDTO;
import com.teamit.smartgarage.services.contracts.VehicleModelService;
import com.teamit.smartgarage.utils.mappers.VehicleModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/garage/models")
public class VehicleModelRESTController {

    private final VehicleModelService vehicleModelService;
    private final VehicleModelMapper vehicleModelMapper;

    public VehicleModelRESTController(VehicleModelService vehicleModelService, VehicleModelMapper vehicleModelMapper) {
        this.vehicleModelService = vehicleModelService;
        this.vehicleModelMapper = vehicleModelMapper;
    }

    @GetMapping
    public List<VehicleModel> getAll(@RequestParam(required = false) Optional<String> search) {
        if (search.isPresent()) {
            return vehicleModelService.search(search);
        }
        return vehicleModelService.getAll();
    }

    @GetMapping("/filter")
    public List<VehicleModel> filter(@RequestParam(required = false) Optional<String> brand,
                                     @RequestParam(required = false) Optional<String> sort,
                                     @RequestParam(required = false) Optional<String> order) {
        try {
            return vehicleModelService.filter(brand, sort, order);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public VehicleModel getById(@PathVariable long id) {
        try {
            return vehicleModelService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void create(@Valid @RequestBody VehicleModelDTO vehicleModelDTO) {
        try {
            VehicleModel vehicleModel = vehicleModelMapper.fromDTO(vehicleModelDTO);
            vehicleModelService.create(vehicleModel);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@PathVariable long id, @Valid @RequestBody VehicleModelDTO vehicleModelDTO) {
        try {
            VehicleModel vehicleModelToUpdate = vehicleModelMapper.fromDTO(id, vehicleModelDTO);
            vehicleModelService.update(vehicleModelToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        try {
            vehicleModelService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
