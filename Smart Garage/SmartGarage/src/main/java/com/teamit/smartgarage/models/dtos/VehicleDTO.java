package com.teamit.smartgarage.models.dtos;

public class VehicleDTO extends VehicleBaseDTO {
    /**
     * Used for REST Controller testing
     */

    private String model;

    private long ownerId;

    public VehicleDTO() {
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }
}