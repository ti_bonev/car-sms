package com.teamit.smartgarage.utils.mappers;

import com.mifmif.common.regex.Generex;
import com.teamit.smartgarage.exceptions.IncorrectEntryException;
import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.dtos.EmailDTO;
import com.teamit.smartgarage.models.dtos.PasswordDTO;
import com.teamit.smartgarage.models.dtos.UserDTO;
import com.teamit.smartgarage.models.dtos.UserDTOOut;
import com.teamit.smartgarage.services.contracts.UserRoleService;
import com.teamit.smartgarage.services.contracts.UserService;
import com.teamit.smartgarage.services.contracts.VehicleService;
import com.teamit.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private final UserService userService;
    private final UserRoleService userRoleService;
    private final VisitService visitService;
    private final VehicleService vehicleService;

    @Autowired
    public UserMapper(UserService userService,
                      UserRoleService userRoleService,
                      VisitService visitService,
                      VehicleService vehicleService) {
        this.userService = userService;
        this.userRoleService = userRoleService;
        this.visitService = visitService;
        this.vehicleService = vehicleService;
    }

    public User fromDTO(UserDTO userDTO) {
        User user = new User();
        user.setUsername(userDTO.getEmail());
        user.setPassword(generatePassword());
        dtoToObject(userDTO, user);
        user.setRole(userRoleService.getByName("client"));
        return user;
    }

    public User fromDTO(long id, UserDTO userDTO) {
        User userToUpdate = userService.getByID(id);
        dtoToObject(userDTO, userToUpdate);
        return userToUpdate;
    }

    private void dtoToObject(UserDTO userDTO, User user) {
        user.setEmail(userDTO.getEmail());
        user.setPhone(userDTO.getPhone());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
    }

    private String generatePassword() {
        Generex generex = new Generex("[a-z][0-9][@$!%*?&][A-Z][A-Za-z0-9@$!%*?&]{4}");
        return generex.random();
    }

    public UserDTO toDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(user.getEmail());
        userDTO.setPhone(user.getPhone());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        return userDTO;
    }

    public User fromEmailDTO(EmailDTO emailDTO) {
        User user = userService.getByEmail(emailDTO.getEmail());
        user.setPassword(generatePassword());
        return user;  // throws EntityNotFoundException
    }

    public void updatePassword(User user, PasswordDTO passwordDTO) {
        // check if old password == existing in db
        if (!user.getPassword().equals(passwordDTO.getOldPassword()))
            throw new IncorrectEntryException("Incorrect existing password entered.");
        // check if new password typed correctly twice
        if (!passwordDTO.getNewPassword().equals(passwordDTO.getNewPasswordConfirm()))
            throw new IncorrectEntryException("Incorrect input. Different values are typed.");
        user.setPassword(passwordDTO.getNewPassword());
    }

    public UserDTOOut toDTOOut(User user) {
        UserDTOOut userDTOOut = new UserDTOOut();
        userDTOOut.setId(user.getId());
        userDTOOut.setUsername(user.getUsername());
        userDTOOut.setPassword(user.getPassword());
        userDTOOut.setEmail(user.getEmail());
        userDTOOut.setPhone(user.getPhone());
        userDTOOut.setFirstName(user.getFirstName());
        userDTOOut.setLastName(user.getLastName());
        userDTOOut.setRole(user.getRole());
        userDTOOut.setVisits(visitService.getUserVisits(user));
        userDTOOut.setVehicles(vehicleService.getUserVehicles(user));
        return userDTOOut;
    }
}