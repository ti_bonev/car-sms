package com.teamit.smartgarage.repositories.contracts;

import com.teamit.smartgarage.models.UserRole;

public interface UserRoleRepository extends BaseReadRepository<UserRole> {
}
