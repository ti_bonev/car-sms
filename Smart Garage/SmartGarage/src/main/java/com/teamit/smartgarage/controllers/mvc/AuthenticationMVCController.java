package com.teamit.smartgarage.controllers.mvc;

import com.teamit.smartgarage.exceptions.AuthenticationFailureException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.dtos.EmailDTO;
import com.teamit.smartgarage.models.dtos.LogInDTO;
import com.teamit.smartgarage.services.contracts.UserService;
import com.teamit.smartgarage.utils.helpers.AuthenticationHelper;
import com.teamit.smartgarage.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/v1/auth")
public class AuthenticationMVCController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public AuthenticationMVCController(AuthenticationHelper authenticationHelper,
                                       UserService userService,
                                       UserMapper userMapper) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping("/login")
    public String showLogInPage(Model model) {
        model.addAttribute("login", new LogInDTO());
        return "auth-login";
    }

    @PostMapping("/login")
    public String logIn(@Valid @ModelAttribute("login") LogInDTO logInDTO,
                        BindingResult bindingResult,
                        HttpSession session) {
        if (bindingResult.hasErrors())
            return "auth-login";
        try {
            authenticationHelper.verifyAuthentication(logInDTO.getUsername(), logInDTO.getPassword());
            session.setAttribute("currentUser", logInDTO.getUsername());
            User currentUser = userService.getByUsername(logInDTO.getUsername());
            if (userService.isClient(currentUser))
                return "redirect:/v1/private";
            else
                return "redirect:/v1/vehicles";
        } catch (AuthenticationFailureException e) {
            if (e.getMessage().equals("Wrong username"))
                bindingResult.rejectValue("username", "auth.error", e.getMessage());
            if (e.getMessage().equals("Wrong password"))
                bindingResult.rejectValue("password", "auth.error", e.getMessage());
            return "auth-login";
        }
    }

    @GetMapping("/logout")
    public String logOut(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/v1/home";
    }

    @GetMapping("/password")
    public String showForgottenPasswordPage(Model model) {
        model.addAttribute("email", new EmailDTO());
        return "auth-forgotten-password";
    }

    @PostMapping("/password")
    public String sendPassword(@Valid @ModelAttribute("email") EmailDTO emailDTO,
                               BindingResult errors,
                               Model model) {
        if (errors.hasErrors())
            return "auth-forgotten-password";
        try {
            User user = userMapper.fromEmailDTO(emailDTO);
            User currentUser = userService.getAdmin();
            // update & send User password will be best to pass both as transaction
            userService.update(user, currentUser);
            userService.sendNewPasswordEmail(user);
            return "redirect:/v1/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}