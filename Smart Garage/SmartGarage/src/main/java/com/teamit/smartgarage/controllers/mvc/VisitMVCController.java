package com.teamit.smartgarage.controllers.mvc;

import com.teamit.smartgarage.exceptions.CurrencyConversionException;
import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.Assistance;
import com.teamit.smartgarage.models.Vehicle;
import com.teamit.smartgarage.models.Visit;
import com.teamit.smartgarage.models.dtos.*;
import com.teamit.smartgarage.services.contracts.AssistanceService;
import com.teamit.smartgarage.services.contracts.VisitService;
import com.teamit.smartgarage.utils.enums.Currencies;
import com.teamit.smartgarage.utils.mappers.VisitMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/v1/visits")
public class VisitMVCController {

    private final VisitService visitService;
    private final VisitMapper visitMapper;
    private final AssistanceService assistanceService;

    @Autowired
    public VisitMVCController(VisitService visitService,
                              VisitMapper visitMapper,
                              AssistanceService assistanceService) {
        this.visitService = visitService;
        this.visitMapper = visitMapper;
        this.assistanceService = assistanceService;
    }

    @ModelAttribute("searchEndPoint")
    public String populateSearchEndPoint() {
        return "/v1/visits/search";
    }

    @ModelAttribute("assistance")
    public EntityIdDTO populateAssistance() {
        return new EntityIdDTO();
    }

    @ModelAttribute("assistances")
    public List<Assistance> populateAssistances() {
        return assistanceService.getAll();
    }

    @ModelAttribute("currencies")
    public List<Currencies> populateSortOptions() {
        return Arrays.asList(Currencies.values());
    }

    @GetMapping
    public String showVisitsPage(Model model) {
        model.addAttribute("visits", visitService.getAll());
        return "visit-all";
    }

    @GetMapping("/{id}")
    public String showVisitPage(@PathVariable long id,
                                Model model) {
        try {
            VisitDTOOut visitDTOOut = visitMapper.toDTOOut(visitService.getById(id));
            model.addAttribute("visit", visitDTOOut);
            model.addAttribute("toCurrency", new CurrencyDTO());
            return "visit";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/search")
    public String searchVisit(@Valid @ModelAttribute("search") SearchDTO searchDTO,
                              BindingResult bindingResult,
                              Model model) {
        if (bindingResult.hasErrors())
            return "visit-all";
        var searchResult = visitService.search(
                Optional.ofNullable(searchDTO.getKeyword().isBlank() ? null : searchDTO.getKeyword())
        );
        model.addAttribute("visits", searchResult);
        return "visit-all";
    }

    @PostMapping("/new")
    public String createVisit(@Valid @ModelAttribute("vehicle") Vehicle vehicle,
                              Model model) {
        VisitDTO visitDTO = new VisitDTO();
        visitDTO.setVehicleId(vehicle.getId());
        try {
            Visit visit = visitMapper.fromDTO(visitDTO);
            visitService.create(visit);
            return "redirect:/v1/visits/" + visit.getId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/add")
    public String addAssistanceToVisit(@PathVariable long id,
                                       @Valid @ModelAttribute("assistance") EntityIdDTO entityIdDTO,
                                       Model model) {
        try {
            Assistance assistance = assistanceService.getById(entityIdDTO.getId());
            visitService.addServiceToVisit(id, assistance);
            return "redirect:/v1/visits/" + id;
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/v1/visits/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/remove/{assistanceID}")
    public String removeAssistanceFromVisit(@PathVariable long id,
                                            @PathVariable long assistanceID,
                                            Model model) {
        try {
            Assistance assistance = assistanceService.getById(assistanceID);
            visitService.removeServiceFromVisit(id, assistance);
            return "redirect:/v1/visits/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteVisit(@PathVariable long id,
                              Model model) {
        //todo soft delete
        try {
            visitService.delete(id);
            return "redirect:/v1/visits";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/complete")
    public String completeVisit(@PathVariable long id,
                                Model model) {
        try {
            visitService.closeVisit(id);
            return "redirect:/v1/visits/" + id;
        } catch (EntityNotFoundException | UnsupportedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/currency")
    public String convertCurrency(@PathVariable long id,
                                  @Valid @ModelAttribute("toCurrency") CurrencyDTO currencyDTO,
                                  Model model) {
        try {
            Visit visit = visitService.getById(id);
            String currency = currencyDTO.getCurrency();
            VisitDTOOut visitDTOOut = visitMapper.toDTOOut(visit, currency);
            model.addAttribute("visit", visitDTOOut);
            model.addAttribute("toCurrency", new CurrencyDTO());
            return "visit";
        } catch (EntityNotFoundException | UnsupportedOperationException | CurrencyConversionException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/send")
    public String sendReport(@PathVariable long id,
                             @Valid @ModelAttribute("visit") VisitDTOOut visitDTOOut,
                             Model model) {
        try {
            Visit visit = visitService.getById(id);
            visitService.sendVisitReportEmail(visit);
            return "redirect:/v1/visits/" + id;
        } catch (EntityNotFoundException | UnsupportedOperationException | CurrencyConversionException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}