package com.teamit.smartgarage.repositories.contracts;

public interface BaseCRUDRepository<T> extends BaseReadRepository<T> {

    void create(T entity);

    void update(T entity);

    void delete(long id);

}