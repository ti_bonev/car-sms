package com.teamit.smartgarage.models.dtos;

import javax.validation.constraints.Pattern;

import static com.teamit.smartgarage.models.User.PASSWORD_CHECK_PATTERN;
import static com.teamit.smartgarage.models.User.PASSWORD_ERROR_MSG;

public class PasswordDTO {

    @Pattern(regexp = PASSWORD_CHECK_PATTERN,
            message = PASSWORD_ERROR_MSG)
    private String oldPassword;

    @Pattern(regexp = PASSWORD_CHECK_PATTERN,
            message = PASSWORD_ERROR_MSG)
    private String newPassword;

    @Pattern(regexp = PASSWORD_CHECK_PATTERN,
            message = PASSWORD_ERROR_MSG)
    private String newPasswordConfirm;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordConfirm() {
        return newPasswordConfirm;
    }

    public void setNewPasswordConfirm(String newPasswordConfirm) {
        this.newPasswordConfirm = newPasswordConfirm;
    }
}