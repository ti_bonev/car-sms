package com.teamit.smartgarage.controllers.rest;

import com.teamit.smartgarage.exceptions.DuplicateEntityException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.exceptions.UnauthorizedOperationException;
import com.teamit.smartgarage.models.Assistance;
import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.models.Visit;
import com.teamit.smartgarage.models.dtos.ServiceToVisitDTO;
import com.teamit.smartgarage.models.dtos.VisitDTO;
import com.teamit.smartgarage.services.contracts.AssistanceService;
import com.teamit.smartgarage.services.contracts.UserService;
import com.teamit.smartgarage.services.contracts.VisitService;
import com.teamit.smartgarage.utils.mappers.VisitMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/garage/visits")
public class VisitRESTController {

    private final VisitService visitService;
    private final VisitMapper visitMapper;
    private final AssistanceService assistanceService;
    private final UserService userService;

    @Autowired
    public VisitRESTController(VisitService visitService,
                               VisitMapper visitMapper,
                               AssistanceService assistanceService,
                               UserService userService) {
        this.visitService = visitService;
        this.visitMapper = visitMapper;
        this.assistanceService = assistanceService;
        this.userService = userService;
    }

    @GetMapping
    public List<Visit> getAll(@RequestParam(required = false) Optional<String> search) {
        if (search.isPresent()) {
            return visitService.search(search);
        }
        return visitService.getAll();
    }

    @GetMapping("/filter")
    public List<Visit> filter(@RequestParam(required = false) Optional<String> username,
                              @RequestParam(required = false) Optional<String> brand,
                              @RequestParam(required = false) Optional<String> model,
                              @RequestParam(required = false) Optional<String> licensePlate,
                              @RequestParam(required = false) Optional<String> vin,
                              @RequestParam(required = false) Optional<String> dateFrom,
                              @RequestParam(required = false) Optional<String> dateTo,
                              @RequestParam(required = false) Optional<String> sort,
                              @RequestParam(required = false) Optional<String> order) {
        try {
            return visitService.filter(username, brand, model, licensePlate, vin, dateFrom, dateTo, sort, order);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, e.getMessage());
        }
    }

    @GetMapping("/user/{username}")
    public List<Visit> getUserVisits(@PathVariable String username) {
        try {
            User user = userService.getByUsername(username);
            return visitService.getUserVisits(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Visit getById(@PathVariable long id) {
        try {
            return visitService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    //TODO add DuplicateEntityException after visit status is implemented.
    @PostMapping
    public void create(@RequestBody VisitDTO visitDTO) {
        Visit visit = visitMapper.fromDTO(visitDTO);
        visitService.create(visit);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable long id, @RequestBody VisitDTO visitDTO) {
        try {
            Visit visitToUpdate = visitMapper.fromDTO(id, visitDTO);
            visitService.update(visitToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/services")
    public void addService(@PathVariable long id, @RequestBody ServiceToVisitDTO serviceToVisitDTO) {
        try {
            Assistance assistanceToAdd = assistanceService.getById(serviceToVisitDTO.getId());
            if (serviceToVisitDTO.getAction().equalsIgnoreCase("add")) {
                visitService.addServiceToVisit(id, assistanceToAdd);
            }
            if (serviceToVisitDTO.getAction().equalsIgnoreCase("remove")) {
                visitService.removeServiceFromVisit(id, assistanceToAdd);
            }
        } catch (DuplicateEntityException | UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/close")
    public void closeVisit(@PathVariable long id) {
        try {
            visitService.closeVisit(id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        try {
            visitService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/total")
    public double getTotalPrice(@PathVariable long id) {
        try {
            Visit visit = visitService.getById(id);
            return visitService.getTotalPrice(visit);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/send_report")
    public void sendVisitReportEmail(@PathVariable long id) {
        try {
            Visit visit = visitService.getById(id);
            visitService.sendVisitReportEmail(visit);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
