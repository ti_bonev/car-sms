package com.teamit.smartgarage.utils.helpers;

import com.teamit.smartgarage.exceptions.AuthenticationFailureException;
import com.teamit.smartgarage.exceptions.EntityNotFoundException;
import com.teamit.smartgarage.models.User;
import com.teamit.smartgarage.services.contracts.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class AuthenticationHelper {
    public static final String AUTHENTICATION_HEADER = "Authentication";
    private final UserService userService;

    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHENTICATION_HEADER))
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "You should log in to view this content.");
        try {
            String username = headers.getFirst(AUTHENTICATION_HEADER);
            return userService.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "There is no user in database with this username.");
        }
    }

    public void verifyAuthentication(String username, String password) {
        try {
            User user = userService.getByUsername(username);
            if (!user.getPassword().equals(password))
                throw new AuthenticationFailureException("Wrong password");
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException("Wrong username");
        }
    }
}