package com.teamit.smartgarage.services.contracts;

import com.teamit.smartgarage.models.UserRole;

import java.util.List;

public interface UserRoleService {

    List<UserRole> getAll();

    UserRole getByID(long id);

    UserRole getByName(String role);

}