-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for smart_garage
DROP DATABASE IF EXISTS `smart_garage`;
CREATE DATABASE IF NOT EXISTS `smart_garage` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smart_garage`;

-- Dumping structure for table smart_garage.services
CREATE TABLE IF NOT EXISTS `services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `services_id_uindex` (`id`),
  UNIQUE KEY `services_title_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.services: ~25 rows (approximately)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`id`, `name`, `price`) VALUES
	(1, 'Braking System Repair', 60),
	(2, 'Chassis and Suspension Repair', 15),
	(3, 'Engine Diagnostics', 20),
	(4, 'Computer Diagnostics', 25),
	(5, 'Filter Replacement', 25),
	(6, 'Oil Change', 40),
	(7, 'Suspension Repair', 10),
	(8, 'Steering Repair', 15),
	(9, 'Cooling System Repair', 50),
	(10, 'Engine Repair', 50),
	(11, 'Engine Replacement', 50),
	(12, 'Fuel Injection Repair', 50),
	(13, 'Fuel System Maintenance', 50),
	(14, 'Ignition System Maintenance', 50),
	(15, 'Water Pump Replacement', 50),
	(16, 'Clutch Repair', 50),
	(17, 'Clutch Replacement', 50),
	(18, 'Differential Diagnosis', 50),
	(19, 'Differential Service', 50),
	(20, 'Transmission Fluid Service', 50),
	(21, 'Transmission Repair', 50),
	(22, 'Transmission Replacement', 50),
	(23, 'Tire Balancing', 50),
	(24, 'Tire Replacement', 50),
	(25, 'Wheel Alignment', 15);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

-- Dumping structure for table smart_garage.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `phone_number` varchar(32) NOT NULL,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `user_type_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  UNIQUE KEY `users_id_uindex` (`id`),
  UNIQUE KEY `users_username_uindex` (`username`),
  KEY `users_user_types_id_fk` (`user_type_id`),
  CONSTRAINT `users_user_types_id_fk` FOREIGN KEY (`user_type_id`) REFERENCES `user_roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.users: ~22 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `email`, `phone_number`, `first_name`, `last_name`, `user_type_id`) VALUES
	(1, 'ivaylo_ivanov_1', 'pass4X@0', 'ivaylo_ivanov@sg.com', '0878512348', 'Ivaylo', 'Ivanov', 1),
	(2, 'ti_bonev_2', 'r2*A5I$!', 'ti_bonev@abv.bg', '0878545502', 'Todor', 'Bonev', 1),
	(3, 'fstehr_3', 'pass4X@2', 'fstehr@hotmail.com', '0886956213', 'Vasil', 'Vakareliyski', 2),
	(4, 'llind_4', 'pass4X@3', 'llind@hotmail.com', '0875632958', 'Vitali', 'Halvadzhiev', 2),
	(5, 'lucas67_5', 'pass4X@4', 'lucas67@hotmail.com', '0888965532', 'Aleksandr', 'Gushcherov', 2),
	(7, 'fkreiger_7', 'pass4X@6', 'fkreiger@bechtelar.info', '0883652147', 'Elica', 'Panayotova', 3),
	(8, 'tdicki_8', 'pass4X@7', 'tdicki@hotmail.com', '0885222121', 'Nikolay', 'Dzhumerski', 3),
	(9, 'lharber_9', 'pass4X@8', 'lharber@gmail.com', '0878545652', 'Petr', 'Evstatiev', 3),
	(10, 'christopher48_10', 'pass4X@9', 'christopher48@watsica.info', '0895621478', 'Stanimira', 'Nedelcheva', 3),
	(11, 'nleuschke_11', 'pass4X@10', 'nleuschke@stokes.com', '0884256998', 'Kalin', 'Vezhdarov', 3),
	(13, 'davis.roel_13', 'pass4X@12', 'davis.roel@yahoo.com', '0878569965', 'Yoana', 'Stoyanova', 3),
	(14, 'dharber_14', 'pass4X@13', 'dharber@yahoo.com', '0899665554', 'Adriana', 'Statelova', 3),
	(15, 'ppredovic_15', 'pass4X@14', 'ppredovic@corwin.com', '0897141144', 'Karina', 'Dragolova', 3),
	(16, 'layla.sawayn_16', 'pass4X@15', 'layla.sawayn@braun.com', '0877995995', 'Irina', 'Kirezieva', 3),
	(17, 'schoen.seth_17', 'pass4X@16', 'schoen.seth@hotmail.com', '0858414147', 'Nikolet', 'Stefanova', 3),
	(18, 'norwood96_18', 'pass4X@17', 'norwood96@hotmail.com', '0887236632', 'Radosveta', 'Georgieva', 3),
	(19, 'idicki_19', 'pass4X@18', 'idicki@lockman.com', '0899159159', 'Ekaterina', 'Mihaylova', 3),
	(20, 'kmraz_20', 'pass4X@19', 'kmraz@yahoo.com', '0875414745', 'Gergana', 'Danailova', 3),
	(21, 'iortiz_21', 'pass4X@20', 'iortiz@goodwin.com', '0878545456', 'Lora', 'Stefanova', 3),
	(22, 'audrey.dicki_22', 'pass4X@21', 'audrey.dicki@yahoo.com', '0899125488', 'Kostadin', 'Stoichkov', 3),
	(28, 'dario_oh_28', 'm1!M23!!', 'dario_oh@abv.bg', '0877125169', 'Ivan', 'Ivanov', 3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table smart_garage.user_roles
CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_types_id_uindex` (`id`),
  UNIQUE KEY `user_types_user_type_uindex` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.user_roles: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`id`, `role`) VALUES
	(1, 'admin'),
	(3, 'client'),
	(2, 'employee'),
	(4, 'guest');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

-- Dumping structure for table smart_garage.vehicles
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `license_plate` varchar(16) NOT NULL,
  `vin` varchar(17) NOT NULL,
  `production_year` int(4) NOT NULL,
  `model_id` bigint(20) NOT NULL,
  `owner_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vehicles_id_uindex` (`id`),
  UNIQUE KEY `vehicles_license_plate_uindex` (`license_plate`),
  UNIQUE KEY `vehicles_vin_uindex` (`vin`),
  KEY `vehicles_users_id_fk` (`owner_id`),
  KEY `vehicles_vehicle_models_id_fk` (`model_id`),
  CONSTRAINT `vehicles_users_id_fk` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`),
  CONSTRAINT `vehicles_vehicle_models_id_fk` FOREIGN KEY (`model_id`) REFERENCES `vehicle_models` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.vehicles: ~19 rows (approximately)
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` (`id`, `license_plate`, `vin`, `production_year`, `model_id`, `owner_id`) VALUES
	(2, 'PP1197BH', 'JTEBT14R980069534', 2012, 42, 2),
	(3, 'X2000AX', '5TFAW5F15EX51E610', 2012, 27, 8),
	(4, 'CB6037KA', 'JTEGH20V336012557', 2009, 32, 9),
	(5, 'EH7282BA', '5TBKT42173S401116', 2013, 37, 10),
	(6, 'EB6633AH', 'JTMBD33V965012990', 2015, 32, 11),
	(8, 'TX7337TX', '4T1BG22K61U202474', 2020, 20, 13),
	(9, 'CB3932KB', '2T1BU4EE1BC546825', 2014, 21, 14),
	(10, 'PB5079TT', '5TDXK3DC2BS087205', 2014, 20, 15),
	(11, 'PB1807KC', 'JTMZD31V466007384', 2020, 18, 16),
	(12, 'CB4078BM', '5TDBK3EH2BS069047', 2016, 16, 17),
	(13, 'PB0168MX', '5TFUU4EN3DXD74950', 2014, 28, 15),
	(14, 'PA5460AB', 'JT2CE82L4E3002532', 2012, 32, 18),
	(15, 'PB9189PC', 'JTERU77F03K056653', 2012, 20, 19),
	(16, 'PP7466TP', 'JTDKB20U887811565', 2020, 25, 20),
	(17, 'CT3157PB', '5TFDW5F16DX275480', 2015, 22, 21),
	(18, 'PB3509TP', '5TDBW5G15ES098448', 2016, 32, 22),
	(19, 'Y6644AH', 'JH4CC2650NC000393', 2014, 17, 14),
	(23, 'A1122BC', 'JH4DA9450MS001229', 2012, 15, 28);
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;

-- Dumping structure for table smart_garage.vehicle_brands
CREATE TABLE IF NOT EXISTS `vehicle_brands` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vehicle_brands_brand_name_uindex` (`brand_name`),
  UNIQUE KEY `vehicle_brands_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.vehicle_brands: ~10 rows (approximately)
/*!40000 ALTER TABLE `vehicle_brands` DISABLE KEYS */;
INSERT INTO `vehicle_brands` (`id`, `brand_name`) VALUES
	(1, 'Audi'),
	(2, 'Dacia'),
	(3, 'Ford'),
	(4, 'Kia'),
	(5, 'Opel'),
	(6, 'Peugeot'),
	(7, 'Renault'),
	(8, 'Skoda'),
	(9, 'Toyota'),
	(10, 'Volkswagen');
/*!40000 ALTER TABLE `vehicle_brands` ENABLE KEYS */;

-- Dumping structure for table smart_garage.vehicle_models
CREATE TABLE IF NOT EXISTS `vehicle_models` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(64) NOT NULL,
  `brand_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vehicle_models_id_uindex` (`id`),
  UNIQUE KEY `vehicle_models_model_name_uindex` (`model_name`),
  KEY `vehicle_models_vehicle_brands_id_fk` (`brand_id`),
  CONSTRAINT `vehicle_models_vehicle_brands_id_fk` FOREIGN KEY (`brand_id`) REFERENCES `vehicle_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.vehicle_models: ~53 rows (approximately)
/*!40000 ALTER TABLE `vehicle_models` DISABLE KEYS */;
INSERT INTO `vehicle_models` (`id`, `model_name`, `brand_id`) VALUES
	(1, 'A3', 1),
	(2, 'A4', 1),
	(3, 'A6', 1),
	(4, 'A8', 1),
	(5, 'Q3', 1),
	(6, 'Q5', 1),
	(7, 'Q8', 1),
	(8, 'TT', 1),
	(9, 'Duster', 2),
	(10, 'Jogger', 2),
	(11, 'Logan', 2),
	(12, 'Sandero', 2),
	(13, 'Spring', 2),
	(14, 'Focus', 3),
	(15, 'Mondeo', 3),
	(16, 'S-Max', 3),
	(17, 'Galaxy', 3),
	(18, 'Kuga', 3),
	(19, 'Picanto', 4),
	(20, 'Rio', 4),
	(21, 'Ceed', 4),
	(22, 'Optima', 4),
	(23, 'Astra', 5),
	(24, 'Corsa', 5),
	(25, 'Insignia', 5),
	(26, 'Zafira', 5),
	(27, '208', 6),
	(28, '2008', 6),
	(29, '308', 6),
	(30, '3008', 6),
	(31, '508', 6),
	(32, '5008', 6),
	(33, 'Clio', 7),
	(34, 'Captur', 7),
	(35, 'Megane', 7),
	(36, 'Kadjar', 7),
	(37, 'Laguna', 7),
	(38, 'Scala', 8),
	(39, 'Superb', 8),
	(40, 'Octavia', 8),
	(41, 'Fabia', 8),
	(42, 'Corolla', 9),
	(43, 'Aygo', 9),
	(44, 'Yaris', 9),
	(45, 'Camry', 9),
	(46, 'RAV4', 9),
	(47, 'Highlander', 9),
	(48, 'Tundra', 9),
	(49, 'Polo', 10),
	(50, 'Golf', 10),
	(51, 'Passat', 10),
	(52, 'Tiguan', 10),
	(53, 'Touareg', 10);
/*!40000 ALTER TABLE `vehicle_models` ENABLE KEYS */;

-- Dumping structure for table smart_garage.visits
CREATE TABLE IF NOT EXISTS `visits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `vehicle_id` bigint(20) NOT NULL,
  `date_in` date NOT NULL,
  `date_out` date NOT NULL,
  `is_complete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `visits_id_uindex` (`id`),
  KEY `visits_users_id_fk` (`user_id`),
  KEY `visits_vehicles_id_fk` (`vehicle_id`),
  CONSTRAINT `visits_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `visits_vehicles_id_fk` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.visits: ~7 rows (approximately)
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
INSERT INTO `visits` (`id`, `user_id`, `vehicle_id`, `date_in`, `date_out`, `is_complete`) VALUES
	(12, 2, 2, '2022-04-27', '2022-04-27', 1),
	(13, 2, 2, '2022-04-27', '2022-04-27', 1),
	(14, 2, 2, '2022-04-27', '2022-04-27', 1),
	(15, 2, 2, '2022-04-27', '2022-04-27', 1),
	(16, 19, 15, '2022-04-27', '2022-04-27', 0),
	(17, 19, 15, '2022-04-28', '2022-04-28', 0),
	(27, 28, 23, '2022-04-28', '2022-04-28', 1),
	(28, 2, 2, '2022-04-28', '2022-04-28', 1);
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;

-- Dumping structure for table smart_garage.visits_services
CREATE TABLE IF NOT EXISTS `visits_services` (
  `visit_id` bigint(20) NOT NULL,
  `service_id` bigint(20) NOT NULL,
  KEY `visits_services_services_id_fk` (`service_id`),
  KEY `visits_services_visits_id_fk` (`visit_id`),
  CONSTRAINT `visits_services_services_id_fk` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`),
  CONSTRAINT `visits_services_visits_id_fk` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.visits_services: ~15 rows (approximately)
/*!40000 ALTER TABLE `visits_services` DISABLE KEYS */;
INSERT INTO `visits_services` (`visit_id`, `service_id`) VALUES
	(12, 1),
	(12, 3),
	(12, 8),
	(13, 12),
	(14, 6),
	(14, 1),
	(15, 1),
	(16, 4),
	(17, 7),
	(17, 11),
	(27, 8),
	(27, 5),
	(28, 1),
	(28, 12),
	(28, 15),
	(28, 14);
/*!40000 ALTER TABLE `visits_services` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
