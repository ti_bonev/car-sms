create or replace table services
(
    id          int auto_increment
        primary key,
    title       varchar(64)  not null,
    description varchar(512) not null,
    price       int          not null,
    constraint services_id_uindex
        unique (id),
    constraint services_title_uindex
        unique (title)
);

create or replace table user_types
(
    id        int auto_increment
        primary key,
    user_type varchar(32) not null,
    constraint user_types_id_uindex
        unique (id),
    constraint user_types_user_type_uindex
        unique (user_type)
);

create or replace table users
(
    id           int auto_increment
        primary key,
    username     varchar(32) not null,
    password     varchar(32) not null,
    email        varchar(64) not null,
    phone_number varchar(32) not null,
    first_name   varchar(32) not null,
    last_name    varchar(32) not null,
    user_type_id int         null,
    constraint users_email_uindex
        unique (email),
    constraint users_id_uindex
        unique (id),
    constraint users_username_uindex
        unique (username),
    constraint users_user_types_id_fk
        foreign key (user_type_id) references user_types (id)
);

create or replace table vehicle_brands
(
    id         int auto_increment
        primary key,
    brand_name varchar(64) not null,
    constraint vehicle_brands_brand_name_uindex
        unique (brand_name),
    constraint vehicle_brands_id_uindex
        unique (id)
);

create or replace table vehicle_models
(
    id         int auto_increment
        primary key,
    model_name varchar(64) not null,
    brand_id   int         not null,
    constraint vehicle_models_id_uindex
        unique (id),
    constraint vehicle_models_model_name_uindex
        unique (model_name),
    constraint vehicle_models_vehicle_brands_id_fk
        foreign key (brand_id) references vehicle_brands (id)
);

create or replace table vehicles
(
    id            int auto_increment
        primary key,
    license_plate varchar(16) not null,
    vin           varchar(17) not null,
    model_id      int         not null,
    owner_id      int         not null,
    constraint vehicles_id_uindex
        unique (id),
    constraint vehicles_license_plate_uindex
        unique (license_plate),
    constraint vehicles_vin_uindex
        unique (vin),
    constraint vehicles_users_id_fk
        foreign key (owner_id) references users (id),
    constraint vehicles_vehicle_models_id_fk
        foreign key (model_id) references vehicle_models (id)
);

create or replace table visits
(
    id         int auto_increment
        primary key,
    user_id    int  not null,
    vehicle_id int  not null,
    date_in    date not null,
    date_out   date not null,
    is_complete boolean not null,
    constraint visits_id_uindex
        unique (id),
    constraint visits_users_id_fk
        foreign key (user_id) references users (id),
    constraint visits_vehicles_id_fk
        foreign key (vehicle_id) references vehicles (id)
);

create or replace table visits_services
(
    visit_id   int not null,
    service_id int not null,
    constraint visits_services_services_id_fk
        foreign key (service_id) references services (id),
    constraint visits_services_visits_id_fk
        foreign key (visit_id) references visits (id)
);

