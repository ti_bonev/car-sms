# Smart Garage

by Mighty IT Team 07 – Ivo & Tosho


## Project Details

Smart Garage is a web application used by clients and employees of car service. Employees report requested services and clients see performed services on every visit in their account. It has 3 levels of access. 
The public part has info for the developers, contacts, and login form. 
Client part available after login shows registered client’s vehicles and provided services, a list of all available services, and options to update the account’s details. 
Admin part holds main functionalities. Employees can add a new client to the database. Unique username and password are auto-generated and auto-sent to provided e-mail. For every client, a list of vehicles can be created, and for every vehicle – a list of visits. In Visit Details, employees can add/remove services from all available and convert currency for prices. After hitting finish, the visit is closed for update and a detailed report may be sent to the client’s e-mail. All lists may be searched and filtered. With deleting a client account – all related client data is also deleted, and no info is stored in the database.

## Quick Overview

-  Check the ui-present.gif file with screenshots of the three UI interfaces.
-  Check the smart_garage_schema.jpg in ../SmartGarage/db/ to view db structure

## Configure and run the project on your device

The project is developed on Java 11 with Spring MVC, MySQL, and Thymeleaf. The database is stored locally. To run the project follow these steps:

-  Choose Clone / “Clone with HTTPS” or download the source code as an archive
-  Open project in IDE
-  Create your local application.properties file in ..\SmartGarage\src\main\resources directory. Use your local server credentials to establish a connection. To try “send e-mail option you can configure a connection with your own e-mail host, or use current with account in abv.bg. File content is the following:

```
server.error.include-stacktrace=never
server.error.include-message=always
# DataBase Configuration
database.url=jdbc:mysql://localhost:3306/smart_garage
database.username=<TYPE YOUR USERNAME>
database.password=<TYPE YOUR PASSWORD>
# Mail Server Configuration for abv.bg
spring.mail.host=smtp.abv.bg
spring.mail.port=465
spring.mail.username=mighty_it@abv.bg<USE YOUR E-MAIL>
spring.mail.password=<TYPE YOUR PASSWORD>
spring.mail.properties.mail.smtp.auth=true
spring.mail.properties.mail.smtp.starttls.enable=true
mail.smtp.debug=true
# Swagger configuration
spring.mvc.pathmatch.matching-strategy=ANT_PATH_MATCHER
```

-  Run script “smart_garage_create_schema_insert_data.sql” from ..\SmartGarage\db to create database schema and fill data on your local server
-  Run the project
-  Load http://localhost:8080/ in your browser
-  At the footer of admin panel, on dev icon you can find link to Swagger documentation
-  Enjoy!

## About

This project was developed as final project for graduating Telerik Academy Alpha Java 34 cohort in April 2022. We are Ivaylo Ivanov and Todor Bonev and this is our first project as a team. We are proud of our product as we started learning Java from scratch 6 months ago, we have no developer’s background and wrote every line in the code on our own. We have identified many ways to improve the project and we have a long todo list. In branch tosho you will find the first version of plain html frontend.
